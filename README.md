# e-Tarpunòmetre

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

L'e-Tarpunòmetre és un projecte de Tarpuna ([http://tarpunacoop.org](http://tarpunacoop.org)), Cooperativa d'Iniciatives Sostenibles.

![Tarpuna](imatges/logo-tarpuna-small.png)

![e-Tarpunòmetre 1.0](imatges/etarpunometre-v1.0-01.jpg)

L'e-Tarpunòmetre és un dispositiu de mesura de consum elèctric (potència aparent) i condicions ambientals (temperatura i humitat relativa). Està basat en una placa Wemos D1 semblant a una Arduino Uno però amb un controllador ESP8266. Es pot programar igual que un Arduino (des del mateix entorn de programació: l'IDE d'Arduino) però és més ràpida, té mes capacitat d'emmagatzematge i incorpora Wifi.

![Wemos D1](imatges/wemos_d1_r2_b.jpg)

## Funcionalitats

* Sensor de corrent no invasiu
* Sensor de temperatura i humitat (DHT22 o HDC1080)
* Rellotge en temps real
* WIFI AP o connexió a un router extern
* Configurarió via web
* Emmagatzematge de dades en local
* Exportació de dades en format CSV
* Actualitzacions remotes
* Pantalla LCD amb les dades més importants
* Anell de LEDs per notificacions (potència, temperatura i estat de la WIFI)

## Muntatge

### Components

|Unitats|Component|
|---|---|
|1|Wemos D1 R2|
|1|Connector audio de 2 canales y 5 pines 3F57|
|1|Pinça amperimètrica (30A/1V o 100A/50mA)
|1|Sensor DHT22 d'humitats i temperatura|
|1|Mòdul RTC DS2331 RTC amb interfície I2C|
|1|Pantalla LCD 16x2 amb mòdul I2C|
|1|Anell de 16 LEDs WS2812|
|1|Butó (pushbutton)|
|1|Condensador electrolític 10uF|
|3|Resistència 10kOhm 1%|
|1|Resistència 68Ohm 1% (opcional, veure comentaris més abaix)|
||Cables|

En funció del tipus de pinça amperimètrica que es faci servir pot ser necessari
utilitzar una resistència de càrrega. Si la pinça dóna un corrent com a sortida
(p.e. aquelles que indiquen 100A/50mA) aquesta corrent s'ha de convertir en un
voltatge per tal que el microcontrolador el pugui llegir. En el cas de l'exemple
anterior una resistència de 68 ohms farà que els 50mA de límit màxim es llegeixin
com a 3.4V, per tant cada volt que llegim en el microcontrolador serà equivalent a
29.4A del corrent principal.

Si la pinça té com a sortida un voltatge aquesta resistència de càrrega no serà
necessària. Les pinces d'aques tipus acostumen a estar etiquetades amb una llegenda
semblant a: 30A/1V.

### Connexions

El següent esquema mostra les connexions necessàries entre el diferents components:

![Connexions](imatges/etarpunometre-v1.0-connexions.png)

Bàsicament, el codi del programa defineix les següents connexions (si no es fesin
d'aquesta manara es pot canviar el codi per adaptar-lo):

|Origen|Pin de la placa del controllador|
|---|---|
|DIN anell de LEDs|D5|
|Butó|D6|
|DHT22 dades|D7|
|Pin esquerre del jack d'audio|A0|

A més s'han de connectar els dos senyals de dades I2C del rellotge i la pantalla. Rellotge, pantalla i LCD han d'estar alimentats per la línia de 5V. La resta de sensors poden anar a 3V3.

## Codi

### Preprocessar continguts web (opcional)

L'e-Tarpunòmtere conté un **petit servidor web** per configurar aspectes com la connexió WIFI, el rellotge o calibrar el sensor de corrent, entre d'altres. Els continguts d'aquest servidor es troben a la carpeta 'codi/html', però abans de copiar-los al controlador es preprocessen per minificar-los, unificar-los i comprimir-los. Amb aquest **preprocessament** es passa de 7 arxius amb 172Kb en total a un únic arxiu de menys de 50Kb, d'aquesta manera el microcontrolador és capaç de servir aquests continuguts al navegador d'una manera molt més ràpida.

Per tant, si heu de modificar els contiguts del directori HTML (són arxius HTML, CSS i JS), heu de preprocessar-los abans de tornar a gravar-los al microcontrolador. Per això necessitareu  instal·lar **gulp**. Per instal·lar gulp obriu un terminal o una consola de windows i navegueu fins a la carpeta 'codi/energia' del projecte. Un cop allà escriviu:

```

npm install --only=dev
```

Agafarà les dependències de l'ariux packages.json i les instal·larà en el mateix lloc sota un directori anomenat 'node_modules'. Un cop instal·lat tot el necessari ja podeu preprocessar els continguts de la carpeta 'html' per generar l'arxiu únic a pujar.

```

node node_modules/gulp/bin/gulp.js
```

El resultat és un arxiu 'codi/energia/static/index.html.gz.h' que es compilarà juntament amb la resta del codi.

### Compilar amb l'IDE d'Arduino

#### Descarregar l'IDE

Visita la pàgina del projecte [Arduino](http://arduino.cc) i des de la secció "Software" busquem el bloc "Download the Arduino IDE" i seleccionem l'instal·lador corresponent al vostre sistema operatiu.

![Preferències Arduino IDE](imatges/arduino-ide-01.jpg)

#### Preparar l'IDE

El primer pas és instal·lar el suport per ESP8266 a l'IDE d'Arduino a través del Gestor de Plaques. Aquestes instruccions estan adaptades de la documentació del projecte Arduino Core for ESP8266 disponible en aquesta adreça: [https://github.com/esp8266/Arduino/blob/master/doc/installing.md](https://github.com/esp8266/Arduino/blob/master/doc/installing.md).

- Obre l'IDE d'Arduino i la finestra de **preferències**.
- Copiar el text ```http://arduino.esp8266.com/stable/package_esp8266com_index.json``` en el quadre **URLs adicionals del Gestor de Plaques**. Si ja hi hagués quelcom escrit afegeix-lo al final separat per una coma.
- Tanquem prement "OK"
- Obrim el **gestor de plaques** des de "Eines > Gestor de Plaques" i busquem la opció *esp8266*.
- Selecciona la darrera versió del desplegable i clica **Instal·la**

![Preferències Arduino IDE](imatges/arduino-ide-02.png)

![Instal·lació suport ESP8266](imatges/arduino-ide-03.png)

#### Instal·lar les llibreries

El projecte fa servir diferents llibreries desenvolupades per encapsular funcionalitats i interactuar amb els diferents sensors o actuadors. Aquestes llibreries les pots trobar comprimides en format ZIP a la carpeta "llibreries" del projecte.

Algunes d'aquestes llibreries són estàndard i es poden instal·lar directament des del **gestor de llibreries** (Esbós > Inclou llibreria > Gestiona les llibreries). De totes maneres totes es poden instal·lar des de la opció **Afegir llibreria .ZIP** del mateix submenú.

![Llibreries 1](imatges/arduino-ide-04.jpg)

![Llibreries 2](imatges/arduino-ide-05.png)

#### Compilar i pujar el codi

Obrim el codi del projecte fent doble click al fitxer 'codi/energia/energia.ino'. S'hauria d'obrir l'IDE d'Arduino amb tots els arxius necessaris, cadascun en una pestanya diferent.

El primer és indicar a l'IDE quina és la placa amb la que treballarem:

1. Selecciona a "Eines > Tarja" la placa **WeMos D1 R2 & mini**
1. Selecciona a "Eines > Flash Size" la opció **4M (3M SPIFFS)**
1. Connecta la placa via USB a l'ordinador i selecciona el **port** a "Eines > Port", normalment és l'únic disponible ("COM + número" a Windows; "/dev/ttyUSB + número" a Linux)

![Opcions de compilació](imatges/arduino-ide-06.jpg)

Un cop fet això només cal **compilar** prement el primer botó de la barra d'eines i **pujar** el programa a la placa prement el segon.

![Compilar i pujar](imatges/arduino-ide-07.png)

## Ús

Podeu trobar tota la informació disponible sobre configuració i ús a la pàgina web:   http://tarpunacoop.org/e-tarpunometre

### Configuració

En arrencar el dispositiu crea una xarxa WIFI pròpia de nom ENERGIA_XXXXXX (on X és un caràcter alfanumèric). La clau per connectar-se a aquesta xarxa és "fibonacci".

Un cop connectats poder obrir el navegador i anar a la direcció [http://192.168.4.1](http://192.168.4.1). Ens demanarà un usuari i clau d'accés. L'usuari per defecte és "admin" i la clau d'accés novament "fibonacci".

El primer cop que hi accedim ens obligarà a canviar la clau d'accés per defecte per una paraula de 8 caracters amb majúscules, minúscules i números. A partir d'aquí aquesta nova clau substitueix a la de "fibonacci" a tot arreu. És possible que torni a demanar les credencial per accedir. L'usuari torna a ser "admin" però la clau serà la que acabem d'entrar.

Un cop a l'interfície de l'aplicació podem:

1. Consultar les dades en temps real (DADES ACTUALS)
1. Descarregar les dades històriques (REGISTRE DADES)
1. Configurar el dispositiu (CONFIGURACIÓ)
1. Definir la connexió WIFI (CONNEXIÓ WIFI)

![e-Tarpunòmetre - Dades](imatges/etarpunometre-web-01.jpg)

![e-Tarpunòmetre - Registre](imatges/etarpunometre-web-02.jpg)

![e-Tarpunòmetre - Configuració](imatges/etarpunometre-web-03.jpg)

![e-Tarpunòmetre - Wifi](imatges/etarpunometre-web-04.jpg)

Per que els canvis quedin registrats és obligatori preme el botó **DESA** del menú esquerra (o al desplegable si ho feu des d'un dispositiu mòbil).

### Interfície físic

L'eTarpunòmetre diposa de dos elements de notificacions (pantalla LCD i anell de LEDs) i un botó. El botó permet definir què es mostra a l'anell de LEDs: temperatura, potència o res. Totes les dades es mostren a la pantalla LCD que va rotant entre diferents missatges. Aquestes dades inclouen temperatura, humitat, potència aparent, IP del dispositu o nom de la xarxa WIFI que ha creat.

## Llicència

Aquesta obra està subjecte a la llicència de Reconeixement-NoComercial-CompartirIgual 4.0 Internacional de Creative Commons. Si voleu veure una còpia d'aquesta llicència accediu a http://creativecommons.org/licenses/by-nc-sa/4.0/ o envieu una carta sol·licitant-la a Creative Commons,  PO Box 1866, Mountain View, CA 94042, USA.
