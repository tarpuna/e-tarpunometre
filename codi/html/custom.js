var websock;
var timer;
var webhost;

function validateForm(form) {
    return true;
}

function valueSet(data, name, value) {
    for (var i in data) {
        if (data[i]['name'] == name) {
            data[i]['value'] = value;
            return;
        }
    }
    data.push({'name': name, 'value': value});
}

function zeroPad(number, positions) {
    return ("0".repeat(positions) + number).slice(-positions);
}

function doUpdate() {

    var form = $("#formSave");

    if (validateForm(form)) {

        // Get data
        var data = form.serializeArray();

        // Post-process
        delete(data['filename']);
        $("input[type='checkbox']").each(function() {
            var name = $(this).attr("name");
            if (name) {
                valueSet(data, name, $(this).is(':checked') ? 1 : 0);
            }
        });

        websock.send(JSON.stringify({'config': data}));

    }

    return false;

}

function doReset() {
    var response = window.confirm("Estàs segur de voler reiniciar el dispositiu?");
    if (response == false) return false;
    websock.send(JSON.stringify({'action': 'reset'}));
    return false;
}

function doReconnect() {
    var response = window.confirm("Estàs segur de voler desconnnectar el dispositiu de l'actual xarxa Wifi?");
    if (response == false) return false;
    websock.send(JSON.stringify({'action': 'reconnect'}));
    return false;
}

function doClear() {
    $.ajax({
        'method': 'GET',
        'url': '/clear',
        'dataType': 'json'
    }).done(function(data) {
        var iframe = document.getElementById("logs");
        iframe.src = iframe.src;
    });
}

function showPanel() {
    $(".panel").hide();
    $("#" + $(this).attr("data")).show();
    if ($("#layout").hasClass('active')) toggleMenu();
    $("input[type='checkbox']").iphoneStyle("calculateDimensions").iphoneStyle("refresh");
};

function toggleMenu() {
    $("#layout").toggleClass('active');
    $("#menu").toggleClass('active');
    $("#menuLink").toggleClass('active');
}

function processData(data) {

    // title
    if ("hostname" in data) {
        document.title = data.hostname + " - E-TARPUNÒMETRE";
    }

    Object.keys(data).forEach(function(key) {

        // Messages
        if (key == "message") {
            window.alert(data.message);
            return;
        }

        // Enable options
        if (key.endsWith("Visible")) {
            var module = key.slice(0,-7);
            $(".module-" + module).show();
            return;
        }

        // Pre-process
        if (key == "network") {
            data.network = data.network.toUpperCase();
        }
        if (key == "ntpStatus") {
            data.ntpStatus = data.ntpStatus ? "SYNC'D" : "NOT SYNC'D";
        }

        // Look for INPUTs
        var element = $("input[name=" + key + "]");
        if (element.length > 0) {
            if (element.attr('type') == 'checkbox') {
                element
                    .prop("checked", data[key])
                    .iphoneStyle("refresh");
            } else if (element.attr('type') == 'radio') {
                element.val([data[key]]);
            } else {
                var pre = element.attr("pre") || "";
                var post = element.attr("post") || "";
                element.val(pre + data[key] + post);
            }
            return;
        }

        // Look for SPANs
        var element = $("span[name=" + key + "]");
        if (element.length > 0) {
            var pre = element.attr("pre") || "";
            var post = element.attr("post") || "";
            element.html(pre + data[key] + post);
            return;
        }

        // Look for SELECTs
        var element = $("select[name=" + key + "]");
        if (element.length > 0) {
            element.val(data[key]);
            return;
        }

    });

    // WIFI
    var groups = $("#panel-wifi .pure-g");
    for (var i in data.wifi) {
        var wifi = data.wifi[i];
        Object.keys(wifi).forEach(function(key) {
            var id = "input[name=" + key + "]";
            if ($(id, groups[i]).length) $(id, groups[i]).val(wifi[key]);
        });
    };

}

function zeroPad(number, positions) {
    return ("0".repeat(positions) + number).slice(-positions);
}

function updateTime() {
    var d = new Date();
    var value =
        zeroPad(d.getDate(), 2) + "/" +
        zeroPad(d.getMonth() + 1, 2) + "/" +
        zeroPad(d.getFullYear(), 4) + " " +
        zeroPad(d.getHours(), 2) + ":" +
        zeroPad(d.getMinutes(), 2) + ":" +
        zeroPad(d.getSeconds(), 2);
    $("input[name='timeNow']").val(value);
}

function getJson(str) {
    try {
        return JSON.parse(str);
    } catch (e) {
        return false;
    }
}

function connect(host) {

    if (typeof host === 'undefined') {
        host = window.location.href.replace('#', '');
    } else {
        if (!host.startsWith("http")) {
            host = 'http://' + host + '/';
        }
    }
    webhost = host;
    wshost = host.replace('http', 'ws') + 'ws';

    if (websock) websock.close();
    websock = new WebSocket(wshost);
    websock.onopen = function(evt) {
        console.log("Connected");
    };
    websock.onclose = function(evt) {
        console.log("Disconnected");
    };
    websock.onerror = function(evt) {
        console.log("Error: ", evt);
    };
    websock.onmessage = function(evt) {
        var data = getJson(evt.data);
        if (data) processData(data);
    };
}

function init() {

    $("#menuLink").on('click', toggleMenu);
    $(".button-update").on('click', doUpdate);
    $(".button-reset").on('click', doReset);
    $(".button-reconnect").on('click', doReconnect);
    $(".button-clear").on('click', doClear);
    $(".pure-menu-link").on('click', showPanel);

    var host = window.location.hostname;
    var port = location.port;

    $.ajax({
        'method': 'GET',
        'url': 'http://' + host + ':' + port + '/auth'
    }).done(function(data) {
        connect();
    }).fail(function(){
    });

    timer = setInterval(updateTime, 1000);

}

$(init);
