#!/bin/bash

DESTINATION=arduino
mkdir -p $DESTINATION/libraries

# ------------------------------------------------------------------------------
# Codi
# ------------------------------------------------------------------------------

NAME=energia
rm $NAME/*.gch
zip -q -9 -r $NAME.zip $NAME
mv $NAME.zip $DESTINATION/

# ------------------------------------------------------------------------------
# HTML
# ------------------------------------------------------------------------------

NAME=html
zip -q -9 -r $NAME.zip $NAME
mv $NAME.zip $DESTINATION/

# ------------------------------------------------------------------------------
# Llibreries
# ------------------------------------------------------------------------------

pushd $DESTINATION/libraries
rm -rf tmp
rm -rf *.zip
mkdir -p tmp
cp -r ../../.piolibdeps/* tmp/
pushd tmp

rm -rf ESPAsyncTCP_ID305

rename 's/ /_/g' *
rename 's/_ID\d+//g' *

for f in `ls`; do
    zip -q -r -9 ../$f.zip $f --exclude=*.git*
done

popd
rm -rf tmp
popd
