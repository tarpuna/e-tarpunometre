/*

LCD MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C * lcd;

// -----------------------------------------------------------------------------
// Pantalla
// -----------------------------------------------------------------------------

void lcdSetup() {
    unsigned char addresses[] = {0x20, 0x27, 0x3F};
    unsigned char address = i2cFind(3, addresses);
    if (address > 0) {
        Serial.printf("[LCD] LCD a l'adreça 0x%02X\n", address);
        lcd = new LiquidCrystal_I2C(address, LCD_COLUMNS, LCD_ROWS);
        lcd->init();
        lcd->backlight();
    }
}

void lcdShow(const char * message) {

    if (lcd == NULL) return;

    lcd->clear();
    lcd->setCursor(0,0);
    lcd->print(message);

}

void lcdUpdate() {

    if (lcd == NULL) return;

    char buffer[LCD_COLUMNS+1];
    unsigned char lcdStatus = state(STATE_LCD);
    unsigned char ringStatus = state(STATE_RING);
    stateNext(STATE_LCD);

    // Borrem l'LCD
    lcd->clear();

    // Mostrem l'estat en la primera filera
    lcd->setCursor(0,0);
    if (lcdStatus == 0) {

        sprintf(buffer, "%s %s", APP_NAME, APP_VERSION);
        lcd->print(buffer);

    } else if (lcdStatus == 1) {

        // Mostrem la data i la hora actuals a la primera línea de l'LCD
        RtcDateTime ara = rtcDateTime();
        sprintf(buffer, "%02d-%02d-%04d %02d:%02d", ara.Day(), ara.Month(), ara.Year(), ara.Hour(), ara.Minute());
        lcd->print(buffer);

    } else if (lcdStatus == 2) {

        if (!wifiEnabled()) {
            lcd->print("WIFI OFF");
        } else if (WiFi.getMode() == WIFI_AP) {
            lcd->print(getSetting("hostname").c_str());
        } else {
            lcd->print(getIP().c_str());
        }

    } else if (lcdStatus == 3) {

         if (getNumPhases() > 1) {

		char buffer_a1[5];
		char buffer_a2[5];
		char buffer_a3[5];

		if (getCurrentNow(1) < 10.00) {
			dtostrf(getCurrentNow(1), 4, 2, buffer_a1);
		} else {
			dtostrf(getCurrentNow(1), 4, 1, buffer_a1);
		}
		if (getCurrentNow(2) < 10.00) {
			dtostrf(getCurrentNow(2), 4, 2, buffer_a2);
		} else {
			dtostrf(getCurrentNow(2), 4, 1, buffer_a2);
		}
		if (getCurrentNow(3) < 10.00) {
			dtostrf(getCurrentNow(3), 4, 2, buffer_a3);
		} else {
			dtostrf(getCurrentNow(3), 4, 1, buffer_a3);
		}
		sprintf(buffer, "%s %s %s A", buffer_a1, buffer_a2, buffer_a3);
                lcd->print(buffer);

    } else {

              if (ringStatus == 0) {
                #if LED_MODE == LED_MODE_BASIC
                   lcd->print("Monofasic");
                #else
                   lcd->print("LEDs OFF");
                #endif
              }
              if (ringStatus == 1) lcd->print("LEDs=TEMPERATURA");
              if (ringStatus == 2) lcd->print("LEDs=POTENCIA");

         }
    }

    // temperatura, humitat i potència global
    char buffer_t[4];
    dtostrf(getTemperature(), 4, 1, buffer_t);
    sprintf(buffer, "%sC %lu%% %luW", buffer_t, round(getHumidity()), round(getPowerNow(0)));

    lcd->setCursor(0,1);
    lcd->print(buffer);
}

void lcdConnectedMessage() {

    if (lcd == NULL) return;

    lcd->clear();
    lcd->setCursor(0,0);
    lcd->print((char *) getNetwork().c_str());
    lcd->setCursor(0,1);
    lcd->print((char *) getIP().c_str());
}

void hola_lcd() {

    if (lcd == NULL) return;

    char buffer[LCD_COLUMNS+1];
    sprintf(buffer, "%s %s", APP_NAME, APP_VERSION);
    lcd->clear();
    lcd->setCursor(0,0);
    lcd->print(APP_NAME);
    lcd->setCursor(0,1);
    lcd->print("versio ");
    lcd->print(APP_VERSION);
    delay(2000);

}
