/*

FTP MODULE

Copyright (C) 2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include <FS.h>
#include "SyncClient.h"

#define DATA_BUFFER     64
#define CONTROL_TIMEOUT 1000

SyncClient ftp_control;
SyncClient ftp_data;

// -----------------------------------------------------------------------------
// FTP
// -----------------------------------------------------------------------------

bool _ftpReceive(char * buffer) {

    ftp_control.flush();

    unsigned long start = millis();
    while (millis() - start < CONTROL_TIMEOUT) {
        if (ftp_control.available()) break;
        delay(1);
    }
    if (!ftp_control.available()) return false;

    byte respCode = ftp_control.peek();
    byte pointer = 0;
    byte b;

    while (ftp_control.available()) {
        b = ftp_control.read();
        Serial.write(b);
        if (pointer < DATA_BUFFER-1) {
            buffer[pointer] = b;
            pointer++;
            buffer[pointer] = 0;
        }
    }

    bool error = (respCode >= '4');
    if (error) {
        DEBUG_MSG_P(PSTR("[FTP] QUIT\n"));
        _ftpSend("QUIT", "");
        ftp_control.stop();
        DEBUG_MSG_P(PSTR("[FTP] Desconnectat\n"));
    }

    return !error;

}

size_t _ftpSend(const char * command, const char * param) {
    char buffer[DATA_BUFFER];
    sprintf(buffer, "%s %s\n", command, param);
    size_t len = ftp_control.write((const uint8_t *) &buffer[0], strlen(buffer));
    ftp_control.flush();
    //DEBUG_MSG("> %s %s (%d)\n", command, param, len);
    return len;
}

// -----------------------------------------------------------------------------

bool ftpSendFile(
    const char * host, unsigned char port,
    const char * user, const char * pass,
    const char * filename, const char * asName)
    {

    if (!wifiConnected()) return false;

    char buffer[DATA_BUFFER];

    // Main connection ---------------------------------------------------------

    if (ftp_control.connected()) ftp_control.stop();

    ftp_control.setTimeout(2);
    DEBUG_MSG_P(PSTR("[FTP] Connectant amb %s:%d\n"), host, port);
    if (!ftp_control.connect(host, port)) {
        DEBUG_MSG_P(PSTR("[FTP] ERROR"));
        return false;
    }
    DEBUG_MSG_P(PSTR("[FTP] Connectat!\n"));
    if (!_ftpReceive(buffer)) {
        ftp_control.stop();
        return false;
    }

    DEBUG_MSG_P(PSTR("[FTP] USER %s\n"), user);
    _ftpSend("USER", user);
    if (!_ftpReceive(buffer)) {
        ftp_control.stop();
        return false;
    }

    delay(100);
    DEBUG_MSG_P(PSTR("[FTP] PASS %s\n"), pass);
    _ftpSend("PASS", pass);
    if (!_ftpReceive(buffer)) {
        ftp_control.stop();
        return false;
    }

    DEBUG_MSG_P(PSTR("[FTP] SYST\n"));
    _ftpSend("SYST", "");
    if (!_ftpReceive(buffer)) {
        ftp_control.stop();
        return false;
    }

    DEBUG_MSG_P(PSTR("[FTP] Type I\n"));
    _ftpSend("Type", "I");
    if (!_ftpReceive(buffer)) {
        ftp_control.stop();
        return false;
    }

    DEBUG_MSG_P(PSTR("[FTP] PASV\n"));
    _ftpSend("PASV", "");
    if (!_ftpReceive(buffer)) {
        ftp_control.stop();
        return false;
    }

    char *tStr = strtok(buffer, "(,");
    int array_pasv[6];
    for ( int i = 0; i < 6; i++) {
        tStr = strtok(NULL, "(,");
        array_pasv[i] = atoi(tStr);
        if (tStr == NULL) {
            DEBUG_MSG_P(PSTR("[FTP] PASV - Resposta incorrecta"));
        }
    }

    // Data connection ---------------------------------------------------------

    unsigned int dataPort, hiPort, loPort;
    hiPort = array_pasv[4] << 8;
    loPort = array_pasv[5] & 255;
    dataPort = hiPort | loPort;
    DEBUG_MSG_P(PSTR("[FTP] Data port: %d\n"), dataPort);

    if (ftp_data.connected()) ftp_data.stop();

    ftp_data.setTimeout(2);
    DEBUG_MSG_P(PSTR("[FTP] Connectant amb %s:%d\n"), host, dataPort);
    if (!ftp_data.connect(host, dataPort)) {
        DEBUG_MSG_P(PSTR("[FTP] ERROR"));
        ftp_control.stop();
        return false;
    }

    DEBUG_MSG_P(PSTR("[FTP] STOR %s\n"), asName);
    _ftpSend("STOR", asName);
    if (!_ftpReceive(buffer)) {
        ftp_data.stop();
        ftp_control.stop();
        return false;
    }

    DEBUG_MSG_P(PSTR("[FTP] Llegint arxiu\n"));
    File fh = SPIFFS.open(filename, "r");
    if (!fh) {
        DEBUG_MSG_P(PSTR("[FTP] Error obrint arxiu"));
        ftp_data.stop();
        ftp_control.stop();
        return false;
    }
    if (!fh.seek(0, SeekSet)) {
        DEBUG_MSG_P(PSTR("[FTP] Error llegint arxiu"));
        fh.close();
        ftp_data.stop();
        ftp_control.stop();
        return false;
    }

    DEBUG_MSG_P(PSTR("[FTP] Enviant arxiu\n"));
    {
        char data[DATA_BUFFER];
        unsigned char pointer = 0;
        while (fh.available()) {
            data[pointer++] = fh.read();
            if (pointer == DATA_BUFFER) {
                ftp_data.write((const uint8_t *) &data[0], DATA_BUFFER);
                ftp_data.flush();
                pointer = 0;
                delay(1);
            }
        }
        if (pointer > 0) {
            ftp_data.write((const uint8_t *) &data[0], pointer);
            ftp_data.flush();
        }
    }
    fh.close();

    DEBUG_MSG_P(PSTR("[FTP] Enviat!\n"));
    if (!_ftpReceive(buffer)) {
        ftp_control.stop();
        return true;
    }
    ftp_data.stop();

    DEBUG_MSG_P(PSTR("[FTP] QUIT\n"));
    _ftpSend("QUIT", "");
    if (!_ftpReceive(buffer)) {
        ftp_control.stop();
        return true;
    }

    ftp_control.stop();
    return true;

}
