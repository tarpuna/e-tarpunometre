/*
E tarpunòmetre - Mesurador de consum d'energia elèctrica i
                 paràmetres ambientals de TarpunaCoop

Copyright (C) 2016-2018
Xose Pérez <xose dot perez at gmail dot com>
David Maruny <david at tarpunacoop dot org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "config/version.h"
#include "config/prototypes.h"
#include "config/defaults.h"
#include "config/debug.h"

// -----------------------------------------------------------------------------
// Client Web
// -----------------------------------------------------------------------------

void webUpdate() {

    char buffer[256];
    char buffer_t[8];
    char buffer_c[8];

    dtostrf(getTemperature(), -5, 1, buffer_t);
    dtostrf(getCurrentNow(0), -6, 2, buffer_c);
    snprintf(
        buffer, sizeof(buffer),
        "{\"temperatura\": \"%s\", \"humitat\": \"%d\", \"potencia\": \"%d\", \"corrent\": \"%s\", \"voltatge\": \"%d\"}",
        buffer_t, (int) round(getHumidity()), (int) round(getPowerNow(0)), buffer_c, (int) getVoltage()
    );

    // La informació l'enviem al navegador a través d'un WebSocket (!!)
    // Un websocket és un canal que està permanentment obert entre el navegador
    // i el nostre dispositiu, que fa de servidor, de manera que el navegador
    // s'actualitza instantàniament quan hi ha nova informació
    wsSend(buffer);

}

// -----------------------------------------------------------------------------
// Mètodes d'inici
// -----------------------------------------------------------------------------

void hola() {

    delay(100);
    Serial.printf("%s %s\n", (char *) APP_NAME, (char *) APP_VERSION);
    Serial.printf("%s\n%s\n", (char *) APP_AUTHOR, (char *) APP_WEBSITE);
    Serial.printf("Device: %s\n\n", (char *) DEVICE_NAME);
    Serial.printf("ChipID: %06X\n", ESP.getChipId());
    Serial.printf("CPU frequency: %d MHz\n", ESP.getCpuFreqMHz());
    Serial.printf("Last reset reason: %s\n", (char *) ESP.getResetReason().c_str());
    Serial.printf("Memory size: %d bytes\n", ESP.getFlashChipSize());
    Serial.printf("Free heap: %d bytes\n", ESP.getFreeHeap());
    Serial.printf("Firmware size: %d bytes\n", ESP.getSketchSize());
    Serial.printf("Free firmware space: %d bytes\n", ESP.getFreeSketchSpace());

    Serial.printf("\n");

}

void initSPIFFS() {

    Serial.printf("\n");

    Serial.printf("SPIFFS mount (first time it may take a while...)\n");
    lcdShow("FORMATANT...");
    SPIFFS.begin();
    lcdShow("");
    FSInfo fs_info;
    if (SPIFFS.info(fs_info)) {
        Serial.printf("File system total size: %d bytes\n", fs_info.totalBytes);
        Serial.printf("            used size : %d bytes\n", fs_info.usedBytes);
        Serial.printf("            block size: %d bytes\n", fs_info.blockSize);
        Serial.printf("            page size : %d bytes\n", fs_info.pageSize);
        Serial.printf("            max files : %d\n", fs_info.maxOpenFiles);
        Serial.printf("            max length: %d\n", fs_info.maxPathLength);
    }

    Serial.printf("\n");

}

void setup() {

    Serial.begin(SERIAL_BAUDRATE);
    Serial.println();
    Serial.println();

    hola();

    i2cSetup();
    i2cScanner();

    settingsSetup();
    if (getSetting("hostname").length() == 0) {
        setSetting("hostname", String() + getIdentifier());
    }

    lcdSetup();
    initSPIFFS();
    //registreSetup();

    statesSetup();
    rtcSetup();
    sensorSetup();
    powerSetup();
    ledSetup();
    #ifdef BUTTON_PIN
        buttonSetup();
    #endif

    hola_lcd();

    otaSetup();
    wifiSetup();
    webSetup();
    ntpSetup();
    logSetup();

    #if NOFUSS_SUPPORT
        nofussSetup();
    #endif

}

void loop() {

    registreLoop();
    settingsLoop();
    otaLoop();
    wifiLoop();
    ntpLoop();
    rtcLoop();
    #ifdef BUTTON_PIN
        buttonLoop();
    #endif

    // Comprovem si hem de actualitzar les dades dels sensors
    // Check sensor reading interval
    static unsigned long darrera_mesura = 0;
    unsigned long interval = 1000L * getSetting("readInterval", String(SENSOR_INTERVAL)).toInt();
    if (millis() - darrera_mesura > interval) {

        darrera_mesura = millis();

        #if LED_MODE == LED_MODE_BASIC
            ledShow(true);
        #endif

        sensorLoop();
        powerLoop();
        lcdUpdate();
        webUpdate();

        #if LED_MODE == LED_MODE_BASIC
            ledShow(false);
        #else
            ledShow();
        #endif

    }

    #if NOFUSS_SUPPORT
        nofussLoop();
    #endif

}
