/*

POWERNODE
UTILS MODULE

Copyright (C) 2016 by Tarpuna <xose dot perez at gmail dot com>

*/

// -----------------------------------------------------------------------------
// Utils
// -----------------------------------------------------------------------------

#define countof(a) (sizeof(a) / sizeof(a[0]))
#include <Ticker.h>
Ticker _defer_reset;

String getIdentifier() {
    char buffer[20];
    snprintf_P(buffer, sizeof(buffer), PSTR("%s_%06X"), APP_NAME, ESP.getChipId());
    return String(buffer);
}

String buildTime() {

    const char time_now[] = __TIME__;   // hh:mm:ss
    unsigned int hour = atoi(&time_now[0]);
    unsigned int minute = atoi(&time_now[3]);
    unsigned int second = atoi(&time_now[6]);

    const char date_now[] = __DATE__;   // Mmm dd yyyy
    const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    unsigned int month = 0;
    for ( int i = 0; i < 12; i++ ) {
        if (strncmp(date_now, months[i], 3) == 0 ) {
            month = i + 1;
            break;
        }
    }
    unsigned int day = atoi(&date_now[3]);
    unsigned int year = atoi(&date_now[7]);

    char buffer[20];
    snprintf_P(
        buffer, sizeof(buffer), PSTR("%04d-%02d-%02d %02d:%02d:%02d"),
        year, month, day, hour, minute, second
    );

    return String(buffer);

}

unsigned long getUptime() {

    static unsigned long last_uptime = 0;
    static unsigned char uptime_overflows = 0;

    if (millis() < last_uptime) ++uptime_overflows;
    last_uptime = millis();
    unsigned long uptime_seconds = uptime_overflows * (UPTIME_OVERFLOW / 1000) + (last_uptime / 1000);

    return uptime_seconds;

}

// -----------------------------------------------------------------------------

void reset() {
    ESP.restart();
}

void deferredReset(unsigned long delay) {
    _defer_reset.once_ms(delay, reset);
}

// -----------------------------------------------------------------------------

void printDateTime(const RtcDateTime& dt) {
	char datestring[20];
	snprintf_P(datestring,
			countof(datestring),
			PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
			dt.Month(),
			dt.Day(),
			dt.Year(),
			dt.Hour(),
			dt.Minute(),
			dt.Second() );
    Serial.print("[TIME] ");
    Serial.println(datestring);
}

String cleanHostname(String hostname, unsigned char len) {
    char buffer[len+1];
    hostname.toUpperCase();
    unsigned char count = 0;
    for (unsigned char i=0; i < hostname.length(); i++) {
        unsigned char current = hostname.charAt(i);
        if (((current >= 'A') & (current <= 'Z')) | ((current >= '0') & (current <='9')) | (current == '_')) {
            buffer[count++] = current;
            buffer[count] = 0;
            if (count == len) break;
        }
    }
    return String(buffer);
}

char * ltrim(char * s) {
    char *p = s;
    while ((unsigned char) *p == ' ') ++p;
    return p;
}

double roundTo(double num, unsigned char positions) {
    double multiplier = 1;
    while (positions-- > 0) multiplier *= 10;
    return round(num * multiplier) / multiplier;
}

void nice_delay(unsigned long ms) {
    unsigned long start = millis();
    while (millis() - start < ms) delay(1);
}
