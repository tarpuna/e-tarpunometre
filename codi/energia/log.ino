/*

LOG MODULE

Copyright (C) 2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

// -----------------------------------------------------------------------------
// LOG
// -----------------------------------------------------------------------------

/*

bool _log_enabled = true;

bool registreEnabled() {
    return _log_enabled;
}

void registreConfigure() {
    _log_enabled = (getSetting("logEnabled", 1).toInt() == 1);
}

void registreSetup() {
    registreConfigure();
}

*/

void logSetup() {
    #if FTP_SUPPORT
        ftpConfigure();
    #endif
}

void registreNeteja() {

    FSInfo fs_info;
    if (SPIFFS.info(fs_info)) {

        int freeSize = fs_info.totalBytes - fs_info.usedBytes;
        Serial.printf("[REGISTRE] %d bytes lliures", freeSize);
        if (freeSize < SPIFFS_MIN_SIZE) {

            RtcDateTime ara = rtcDateTime();
            int month = ara.Month();
            int year = ara.Year();

            // Busquem l'arxiu més antic
            char filename[50];
            bool exists = true;
            while (exists) {
                if (--month == 0) {
                    month = 12;
                    --year;
                }
                sprintf(filename, "/logs/%s_%04d%02d.csv", getSetting("hostname").c_str(), year, month);
                exists = SPIFFS.exists(filename);
            }
            if (++month == 13) {
                month = 1;
                ++year;
            }

            // Si estem al mes actual no fem res
            if (month == ara.Month() && year == ara.Year()) return;

            // Esborrem l'arxiu més vell
            sprintf(filename, "/logs/%s_%04d%02d.csv", getSetting("hostname").c_str(), year, month);
            Serial.printf(" - eliminant '%s'", filename);
            SPIFFS.remove(filename);

        }

        Serial.printf("\n");

    }

}

bool registreLinia(char * filename, char * buffer) {

    bool arxiu_existeix = SPIFFS.exists(filename);
    File arxiu = SPIFFS.open(filename, "a+");
    if (arxiu) {

        Serial.print("[REGISTRE] Escrivint dades al registre '");
        Serial.print(filename);
        Serial.println("'");

        // Si l'arxiu no existia la primera línia és la capçalera
        if (arxiu_existeix == false) {
            arxiu.println("DATA_I_HORA;TEMPERATURA;HUMITAT;POTENCIA_W;POTENCIA_MAX_W;ENERGIA_WH;ESTAT");
        };

        arxiu.println(buffer);
        arxiu.close();
        return true;

    } else {

        Serial.print("[REGISTRE] Error obrint l'arxiu de registre '");
        Serial.print(filename);
        Serial.println("'");

    }

    return false;

}

void registreLoop() {

    // Aquests valors sempre tindran el minut anterior al moment de la gravació
    static unsigned char minute = 60; // forcem actualitzar el minut d'entrada
    static unsigned char hour = 0;
    static unsigned char day = 0;
    static unsigned char month = 0;
    static unsigned int year = 0;
    static bool reset = true;

    // Comprovem si ha canviat el minut
    RtcDateTime ara = rtcDateTime();
    if (ara.Minute() == minute || getPowerCount() == 0) return;

    // Avaluem si hem de desar (minut actual és múltiple del període de gravació)

    unsigned long saveInterval = getSetting("saveInterval", String(SAVE_INTERVAL)).toInt() / 60;
    long readingsInterval = saveInterval / getSetting("readInterval", SENSOR_INTERVAL).toInt();

    if ((ara.Minute() % saveInterval == 0) && (year > 0) && (getPowerCount()>=readingsInterval) ) {

        // Comprovem si queda prou espai buit per guardar les dades de tota una setmana
        registreNeteja();

        // Dades a escriure ----------------------------------------------------

        // Hem de cridat powerConsolidate per que calculi els promitjos abans de
        // llegir les dades de potència
        powerConsolidate();

        double temperature = getTemperature();
        int humidity = getHumidity();
        double powerMax[4] = {};
        double power[4] = {};
        power[0]=getPower(0);
        powerMax[0]=getPowerMax(0);
        double energy = power[0] * saveInterval / 60;    // Wh

        char buffer_t[4];
        dtostrf(temperature, -4, 1, buffer_t);
        char detallFases[70];

        if (getNumPhases() > 1) {
            for (int i=1; i<=3; i++) {
              power[i]=getPower(i);
              powerMax[i]=getPowerMax(i);
            }
            snprintf(detallFases, sizeof(detallFases), "P1:%lu P2:%lu P3:%lu PM1:%lu PM2:%lu PM3:%lu [W fases]",
                                                     round(power[1]), round(power[2]), round(power[3]),
                                                     round(powerMax[1]), round(powerMax[2]), round(powerMax[3]));
        }

        // Obtenim la línia del registre
        char linia[120];
        snprintf(
            linia,
            sizeof(linia),
            "%02u-%02u-%04u %02u:%02u:59;%s;%lu;%lu;%lu;%lu;%s",
            day, month, year, hour, minute,
            buffer_t, round(humidity), round(power[0]), round(powerMax[0]), round(energy),
            reset ? "RESET" : detallFases
        );
        reset = false;

        // Separadors decimals a ','
        for (unsigned int i=0; i<strlen(linia); i++) {
            if (linia[i] == '.') linia[i] = ',';
        }

        Serial.printf("[REGISTRE] %s\n", linia);

        // Registre general
        //if (_log_enabled) {

            char filename[50];
            snprintf(filename, sizeof(filename), "/logs/%s_%04d%02d.csv", getSetting("hostname").c_str(), ara.Year(), ara.Month());
            registreLinia(filename, linia);

            #if FTP_SUPPORT
                bool new_file = (month > 0) && (month != ara.Month());
                registreFTP(filename, ara, new_file);
            #endif

        //}

    }

    // Actualitzem les dades de l'hora
    minute = ara.Minute();
    hour = ara.Hour();
    day = ara.Day();
    month = ara.Month();
    year = ara.Year();

}

#if FTP_SUPPORT

bool _ftpFlag = false;
char _ftpFilename[50] = {0};

void ftpConfigure() {
    if (getSetting("ftpHost", FTP_HOST).length() == 0) setSetting("ftpEnabled", 0);
}

void registreFTPFlag(char * filename) {
    Serial.printf("[REGISTRE] Arxiu '%s' marcat per enviar per FTP\n", filename);
    strncpy(_ftpFilename, filename, sizeof(_ftpFilename));
    _ftpFlag = true;
}

void registreFTPFlag() {
    char filename[50];
    RtcDateTime ara = rtcDateTime();
    snprintf(filename, sizeof(filename), "/logs/%s_%04d%02d.csv", getSetting("hostname").c_str(), ara.Year(), ara.Month());
    registreFTPFlag(filename);
}


void registreFTP(char * filename, RtcDateTime now, bool force) {

    bool ftpEnabled = getSetting("ftpEnabled", FTP_ENABLED).toInt() == 1;

    if (ftpEnabled) {

        // Check if we have to send
        if (force) {
            registreFTPFlag(filename);
        } else if (now.Minute() == 0) {
            unsigned char hour = getSetting("ftpHour", FTP_HOUR).toInt();
            if (now.Hour() == hour) registreFTPFlag(filename);
        }

        // Check if flagged to send
        if (_ftpFlag) registreFTPSend();

    }

}

void registreFTPSend() {

    // Destination
    String destination = getSetting("ftpPath", FTP_PATH) + String(&_ftpFilename[6]);

    // Send file
    bool sent = ftpSendFile(
        getSetting("ftpHost", FTP_HOST).c_str(),
        getSetting("ftpPort", FTP_PORT).toInt(),
        getSetting("ftpUser", FTP_USER).c_str(),
        getSetting("ftpPass", FTP_PASS).c_str(),
        (const char *) _ftpFilename, destination.c_str()
    );

    Serial.printf("[REGISTRE] Enviament per FTP => %s\n", sent ? "OK" : "ERROR");

    // On success: update status
    if (sent) _ftpFlag = false;


}

#endif
