/*

BUTTON MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#ifdef BUTTON_PIN

#include <DebounceEvent.h>
DebounceEvent * _button;

// -----------------------------------------------------------------------------
// Botó selector
// -----------------------------------------------------------------------------

void buttonLoop() {

    if (unsigned int event = _button->loop()) {

        if (event == EVENT_RELEASED) {

            if (_button->getEventCount() == 2) {
                createAP();
            }

            if (_button->getEventCount() == 1) {

                if (_button->getEventLength() > BUTTON_LONGCLICK) {
                    DEBUG_MSG_P(PSTR("[BUTTON] Long Click\n"));
                    wifiEnabled(!wifiEnabled());
                    state(STATE_LCD, 1);
                } else {
                    DEBUG_MSG_P(PSTR("[BUTTON] Click\n"));
                    stateNext(STATE_RING);
                    state(STATE_LCD, 3);
                }

                DEBUG_MSG_P(PSTR("[BUTTON] Ring state: %d\n"), state(STATE_RING));
                DEBUG_MSG_P(PSTR("[BUTTON] LCD state : %d\n"), state(STATE_LCD));


                #if LED_MODE == LED_MODE_WS2822
                    ledShow();
                #endif
                lcdUpdate();

                // no blocking delay of 1 second
				unsigned long start = millis();
				while (millis() - start < 1000) delay(1);


            }
        }
    }

}

void buttonSetup() {
    uint8_t btnType = getSetting("btnType", BUTTON_DEFAULT_HIGH).toInt();
    _button = new DebounceEvent(BUTTON_PIN, BUTTON_PUSHBUTTON | BUTTON_SET_PULLUP | btnType);
    Serial.printf("[BUTTON] Botó al pin %d\n", BUTTON_PIN);
}

#endif
