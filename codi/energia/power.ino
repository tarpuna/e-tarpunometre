/*

EMON MODULE

Copyright (C) 2016-2017
Xose Pérez <xose dot perez at gmail dot com>
David Maruny <david at tarpunacoop dot org>

*/

#include <EmonLiteESP.h>
#include <Wire.h>
#include <ADS1115.h>

ADS1115 * _ads;
EmonLiteESP monitor;
bool _useADS1115 = false;

double _ampers[4] = {0};
double _ampersNow[4] = {0};
double _ampersMax[4] = {0};
double _ampersMax_parcial[4] = {0};
double _powerNow[4] = {0};
double _ampers_sum[4] = {0};
double _ampers_parcial[4] = {0};

unsigned int _ampers_count;
unsigned int _ampers_parcial_count;
unsigned int _readings_minute = 1;
unsigned char _num_phases = POWER_PHASES;

// -----------------------------------------------------------------------------
// Monitor d'Energia
// -----------------------------------------------------------------------------

unsigned int getVoltage() {
    return getSetting("emonMains", POWER_MAINS_VOLTAGE).toInt();
}

double getCurrentNow(unsigned char i) {
    return _ampersNow[i];
}

double getCurrent(unsigned char i) {
    return _ampers[i];
}

double getCurrentMax(unsigned char i) {
    return _ampersMax[i];
}

double getPowerNow(unsigned char i) {
    return _ampersNow[i] * getVoltage();
}

double getPower(unsigned char i) {
    return _ampers[i] * getVoltage();
}

double getPowerMax(unsigned char i) {
    return _ampersMax[i] * getVoltage();
}

int getPowerCount() {
    return _ampers_count;
}

int getNumPhases () {
    return _num_phases;
}

bool hasADS1115() {
	return _useADS1115;
}

void powerConsolidate() {
    _ampers[0] = 0.00;
    for (int i=0; i <= 3; i++) {
        _ampers[i] = _ampers_count > 0 ? _ampers_sum[i] / _ampers_count : 0;
        _ampersMax[i] = max(_ampersMax[i], max(_ampers[i], _ampersMax_parcial[i] ));
    }
    _ampers_count = 0;
    _ampers_parcial_count = 0;
}

void powerConfigure() {
    for (int i=0; i <= 3; i++) {
        _ampers_sum[i] = 0.00;
        _ampers_parcial[i] = 0.00;
        _ampersMax[i] = 0.00;
        _ampersMax_parcial[i] = 0.00;
    }
    _readings_minute = 60 / getSetting("readInterval", SENSOR_INTERVAL).toInt();
    monitor.setCurrentRatio(getSetting("emonRatio", String(POWER_CURRENT_RATIO)).toFloat());
    _num_phases = _useADS1115 ? getSetting("numPhases", POWER_PHASES).toInt() : 1;
}

void powerLoop() {

    // Llegim les dades i actualitzem l'acumulador per cada fase
    _ampersNow[0] = 0.00;
    for(unsigned char i = 1; i <= _num_phases; i++) {

        unsigned char port = 0;
        if (_useADS1115) {
            if (i == 1) { port = ADS1115_PORT_F1; }
            if (i == 2) { port = ADS1115_PORT_F2; }
            if (i == 3) { port = ADS1115_PORT_F3; }
            _ads->setMultiplexer(port);  // setMultiplexer  canvia el port
        }

        nice_delay(200);

        _ampersNow[i] = monitor.getCurrent(POWER_SAMPLES, _useADS1115 ? ADS1115_POWER_MODE : INTERNAL_POWER_MODE);
        if (_ampersNow[i] < POWER_MIN_CURRENT) _ampersNow[i] = 0;
        _ampersNow[0] = _ampersNow[0] + _ampersNow[i];

        Serial.printf(
            "[SENSOR F%u] Corrent: %s A  -  Potencia: %d W - Port: %u\n",
            i,
            String(_ampersNow[i], 2).c_str(),
            int(_ampersNow[i] * getVoltage()),
            port == 0 ? port : port - 4
         );
    }

    // Mostra la suma total si hi ha més d'una fase
    if (_num_phases > 1) {
         Serial.printf(
              "[SENSOR F+] Corrent: %s A  -  Potencia: %d W \n",
              String(_ampersNow[0], 2).c_str(),
              int(_ampersNow[0] * getVoltage())
          );
    }

    // Acumulador general

    for (int i = 0; i <= _num_phases; i++) {
        if (_ampers_parcial_count == 0) {
          _ampers_parcial[i] = 0.00;
          _ampersMax_parcial[i] = 0.00;
        }
        if (_ampers_count == 0) {
          _ampersMax[i] = 0.00;
          _ampers_sum[i] = 0.00;
        }
        _ampers_sum[i] += _ampersNow[i];
        _ampers_parcial[i] += _ampersNow[i];
    }
    _ampers_count++;
    _ampers_parcial_count++;

    // Calculem els parcials minutals

    for (int i = 0; i <= _num_phases; i++) {
        _ampersMax_parcial[i] = _ampers_parcial[i] / _ampers_parcial_count ;
    }

    if (_ampers_parcial_count >= _readings_minute) {
        for (int i = 0; i <= _num_phases; i++) {
          _ampersMax[i] = max (_ampersMax[i], _ampersMax_parcial[i]);
        }
        _ampers_parcial_count = 0;
    }

}

void powerSetup() {

    unsigned char addresses[] = {0x48, 0x49, 0x4A, 0x4B};
    unsigned char address = i2cFind(4, addresses);
    _useADS1115 = (address != 0);

    if (_useADS1115) {
        Serial.printf("[POWER] ADS1115 a l'adreça 0x%02X\n", address);
        _ads = new ADS1115(address);
        _ads->initialize();
        _ads->setMode(ADS1115_MODE_SINGLESHOT);
        _ads->setRate(ADS1115_RATE_860);
        _ads->setGain(ADS1115_GAIN);
        _ads->setConversionReadyPinMode();
        _ads->setMultiplexer(ADS1115_PORT_F1);      // setMultiplexer canvia el port
    } else {
        Serial.printf("[POWER] Fent servir ADC intern\n");
    }

    monitor.initCurrent(                             // aquesta funció es crida des de getCurrent   (_currentCallback())
        []() -> unsigned int {
            if (_useADS1115) {
                return _ads->getConversion(true);    // getConversion fa la lectura
            } else {
                return analogRead(A0);
            }
        },
        _useADS1115 ? ADS1115_ADC_BITS : INTERNAL_ADC_BITS,
        _useADS1115 ? ADS1115_REFERENCE_VOLTAGE : INTERNAL_REFERENCE_VOLTAGE,
        getSetting("emonRatio", POWER_CURRENT_RATIO).toFloat()
    );

    // warmup
    monitor.getCurrent(POWER_SAMPLES, _useADS1115 ? ADS1115_POWER_MODE : INTERNAL_POWER_MODE);

    powerConfigure();

}
