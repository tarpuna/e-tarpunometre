/*

WIFI MODULE

Copyright (C) 2016-2018 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include "JustWifi.h"

bool _wifiEnabled = true;

// -----------------------------------------------------------------------------
// WIFI
// -----------------------------------------------------------------------------

String getIP() {
    if (WiFi.getMode() == WIFI_AP) {
        return WiFi.softAPIP().toString();
    }
    return WiFi.localIP().toString();
}

String getNetwork() {
    if (WiFi.getMode() == WIFI_AP) {
        return jw.getAPSSID();
    }
    return WiFi.SSID();
}

void wifiDisconnect() {
    jw.disconnect();
}

void resetConnectionTimeout() {
    jw.resetReconnectTimeout();
}

bool wifiConnected() {
    return jw.connected();
}

bool createAP() {
    jw.disconnect();
    jw.resetReconnectTimeout();
    jw.enableAP(true);
    return true;
}

void wifiConfigure() {

    WiFi.setOutputPower(getSetting("wifiGain", WIFI_GAIN).toFloat());

    jw.setHostname(getSetting("hostname").c_str());
    jw.setSoftAP(getSetting("hostname").c_str(), getSetting("adminPass", ADMIN_PASS).c_str());
    jw.setConnectTimeout(WIFI_CONNECT_TIMEOUT);
    jw.setReconnectTimeout(WIFI_RECONNECT_INTERVAL);
    jw.enableAPFallback(_wifiEnabled);
    jw.cleanNetworks();

    if (!_wifiEnabled) return;

    bool scan = true;
    unsigned char count = 0;

    #if WIFI_CORP_SUPPORT
        if (getSetting("ssidX").length() > 0) {
            jw.addNetwork(
                getSetting("ssidX").c_str(),
                getSetting("passX").c_str()
            );
            scan = false;
            count++;
        }
    #endif

    for (unsigned char i = 0; i< WIFI_MAX_NETWORKS; i++) {
        if (getSetting("ssid" + String(i)).length() == 0) break;
        jw.addNetwork(
            getSetting("ssid" + String(i)).c_str(),
            getSetting("pass" + String(i)).c_str()
        );
        count++;
    }

    jw.enableScan(scan && (count>1));

}

bool wifiEnabled() {
    return _wifiEnabled;
}

void wifiEnabled(bool enabled) {
    _wifiEnabled = enabled;
    wifiConfigure();
    if (_wifiEnabled) {
        jw.turnOn();
    } else {
        jw.turnOff();
    }
}

void wifiDebug(WiFiMode_t modes) {

    bool footer = false;

    if (((modes & WIFI_STA) > 0) && ((WiFi.getMode() & WIFI_STA) > 0)) {

        uint8_t * bssid = WiFi.BSSID();
        DEBUG_MSG_P(PSTR("[WIFI] ------------------------------------- MODE STA\n"));
        DEBUG_MSG_P(PSTR("[WIFI] SSID  %s\n"), WiFi.SSID().c_str());
        DEBUG_MSG_P(PSTR("[WIFI] IP    %s\n"), WiFi.localIP().toString().c_str());
        DEBUG_MSG_P(PSTR("[WIFI] MAC   %s\n"), WiFi.macAddress().c_str());
        DEBUG_MSG_P(PSTR("[WIFI] GW    %s\n"), WiFi.gatewayIP().toString().c_str());
        DEBUG_MSG_P(PSTR("[WIFI] DNS   %s\n"), WiFi.dnsIP().toString().c_str());
        DEBUG_MSG_P(PSTR("[WIFI] MASK  %s\n"), WiFi.subnetMask().toString().c_str());
        DEBUG_MSG_P(PSTR("[WIFI] HOST  http://%s.local\n"), WiFi.hostname().c_str());
        DEBUG_MSG_P(PSTR("[WIFI] BSSID %02X:%02X:%02X:%02X:%02X:%02X\n"),
            bssid[0], bssid[1], bssid[2], bssid[3], bssid[4], bssid[5], bssid[6]
        );
        DEBUG_MSG_P(PSTR("[WIFI] CH    %d\n"), WiFi.channel());
        DEBUG_MSG_P(PSTR("[WIFI] RSSI  %d\n"), WiFi.RSSI());
        footer = true;

    }

    if (((modes & WIFI_AP) > 0) && ((WiFi.getMode() & WIFI_AP) > 0)) {
        DEBUG_MSG_P(PSTR("[WIFI] -------------------------------------- MODE AP\n"));
        DEBUG_MSG_P(PSTR("[WIFI] SSID  %s\n"), getSetting("hostname").c_str());
        DEBUG_MSG_P(PSTR("[WIFI] PASS  %s\n"), getSetting("adminPass").c_str());
        DEBUG_MSG_P(PSTR("[WIFI] IP    %s\n"), WiFi.softAPIP().toString().c_str());
        DEBUG_MSG_P(PSTR("[WIFI] MAC   %s\n"), WiFi.softAPmacAddress().c_str());
        footer = true;
    }

    if (WiFi.getMode() == 0) {
        DEBUG_MSG_P(PSTR("[WIFI] ------------------------------------- MODE OFF\n"));
        DEBUG_MSG_P(PSTR("[WIFI] No connection\n"));
        footer = true;
    }

    if (footer) {
        DEBUG_MSG_P(PSTR("[WIFI] ----------------------------------------------\n"));
    }

}

void wifiDebug() {
    wifiDebug(WIFI_AP_STA);
}

void wifiSetup() {

    wifiConfigure();

    // Message callbacks
    jw.subscribe([](justwifi_messages_t code, char * parameter) {

		#if DEBUG_SERIAL_SUPPORT || DEBUG_UDP_SUPPORT

            // -------------------------------------------------------------------------

            if (code == MESSAGE_SCANNING) {
                DEBUG_MSG_P(PSTR("[WIFI] Scanning\n"));
            }

            if (code == MESSAGE_SCAN_FAILED) {
                DEBUG_MSG_P(PSTR("[WIFI] Scan failed\n"));
            }

            if (code == MESSAGE_NO_NETWORKS) {
                DEBUG_MSG_P(PSTR("[WIFI] No networks found\n"));
            }

            if (code == MESSAGE_NO_KNOWN_NETWORKS) {
                DEBUG_MSG_P(PSTR("[WIFI] No known networks found\n"));
            }

            if (code == MESSAGE_FOUND_NETWORK) {
                DEBUG_MSG_P(PSTR("[WIFI] %s\n"), parameter);
            }

            // -------------------------------------------------------------------------

            if (code == MESSAGE_CONNECTING) {
                DEBUG_MSG_P(PSTR("[WIFI] Connecting to %s\n"), parameter);
            }

            if (code == MESSAGE_CONNECT_WAITING) {
                // too much noise
            }

            if (code == MESSAGE_CONNECT_FAILED) {
                DEBUG_MSG_P(PSTR("[WIFI] Could not connect to %s\n"), parameter);
            }

            if (code == MESSAGE_CONNECTED) {
                wifiDebug(WIFI_STA);
            }

            if (code == MESSAGE_DISCONNECTED) {
                DEBUG_MSG_P(PSTR("[WIFI] Disconnected\n"));
            }

            // -------------------------------------------------------------------------

            if (code == MESSAGE_ACCESSPOINT_CREATING) {
                DEBUG_MSG_P(PSTR("[WIFI] Creating access point\n"));
            }

            if (code == MESSAGE_ACCESSPOINT_CREATED) {
                wifiDebug(WIFI_AP);
            }

            if (code == MESSAGE_ACCESSPOINT_FAILED) {
                DEBUG_MSG_P(PSTR("[WIFI] Could not create access point\n"));
            }

            if (code == MESSAGE_ACCESSPOINT_DESTROYED) {
                DEBUG_MSG_P(PSTR("[WIFI] Access point destroyed\n"));
            }

            // -------------------------------------------------------------------------

            if (code == MESSAGE_WPS_START) {
                DEBUG_MSG_P(PSTR("[WIFI] WPS started\n"));
            }

            if (code == MESSAGE_WPS_SUCCESS) {
                DEBUG_MSG_P(PSTR("[WIFI] WPS succeded!\n"));
            }

            if (code == MESSAGE_WPS_ERROR) {
                DEBUG_MSG_P(PSTR("[WIFI] WPS failed\n"));
            }

            // ------------------------------------------------------------------------

            if (code == MESSAGE_SMARTCONFIG_START) {
                DEBUG_MSG_P(PSTR("[WIFI] Smart Config started\n"));
            }

            if (code == MESSAGE_SMARTCONFIG_SUCCESS) {
                DEBUG_MSG_P(PSTR("[WIFI] Smart Config succeded!\n"));
            }

            if (code == MESSAGE_SMARTCONFIG_ERROR) {
                DEBUG_MSG_P(PSTR("[WIFI] Smart Config failed\n"));
            }

		#endif // DEBUG_SERIAL_SUPPORT || DEBUG_UDP_SUPPORT

        if (code == MESSAGE_ACCESSPOINT_CREATED || code == MESSAGE_CONNECTED) {
            lcdConnectedMessage();
        }

        // Configure mDNS
        #if MDNS_SUPPORT
    	    if (code == MESSAGE_CONNECTED || code == MESSAGE_ACCESSPOINT_CREATED) {
                if (MDNS.begin(WiFi.getMode() == WIFI_AP ? APP_NAME : (char *) WiFi.hostname().c_str())) {
                    MDNS.addService("http", "tcp", getSetting("webPort", WEBSERVER_PORT).toInt());
    	            DEBUG_MSG_P(PSTR("[MDNS] OK\n"));
    	        } else {
    	            DEBUG_MSG_P(PSTR("[MDNS] FAIL\n"));
    	        }
    	    }
        #endif

        // NTP connection reset
        if (code == MESSAGE_CONNECTED) {
            ntpConnect();
        }

    });

}

void wifiLoop() {
    jw.loop();
}
