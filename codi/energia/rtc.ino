/*

RTC MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include <Wire.h>
#include <RtcDS3231.h>

#define RTC_NONE        0
#define RTC_DS3231      1
#define RTC_DS1337      2

RtcDS3231<TwoWire> rtc(Wire);
unsigned int _rtc_chip = RTC_NONE;
unsigned long _rtc_difference = 0;

// -----------------------------------------------------------------------------
// RTC (Rellotge)
// -----------------------------------------------------------------------------

RtcDateTime rtcDateTime() {
    RtcDateTime now;
    if (_rtc_chip == RTC_NONE) {
        now = RtcDateTime(__DATE__, __TIME__);
    } else {
        now = rtc.GetDateTime();
    }
    return now;
}

void rtcDateTime(RtcDateTime d) {
    if (_rtc_chip == RTC_NONE) return;
    rtc.SetDateTime(d);
    rtcResetDrift();
    Serial.printf("[RTC] L'hora de l'RTC és: %s\n", rtcDateTimeString().c_str());
}

String rtcDateTimeString() {
    RtcDateTime d = rtcDateTime();
    char buffer[20];
    snprintf_P(buffer, sizeof(buffer), PSTR("%04d-%02d-%02d %02d:%02d:%02d"),
        d.Year(), d.Month(), d.Day(),
        d.Hour(), d.Minute(), d.Second());
    return String(buffer);
}

unsigned char rtcChip(unsigned char address) {
    Wire.beginTransmission(address);
    Wire.write(DS3231_REG_TEMP);
    Wire.endTransmission();
    Wire.requestFrom(address, DS3231_REG_TEMP_SIZE);
    return (Wire.read() == 0) ? RTC_DS1337 : RTC_DS3231;
}

void rtcResetDrift() {
    _rtc_difference = 0;
}

void rtcLoop() {

    if (_rtc_chip != RTC_DS1337) return;

    // Fix time drifts for DS1337 chips
    static unsigned long _last = 0;

    if ((_last == 0) || (millis() - _last > 60000)) {
        _last = millis();

        RtcDateTime now = rtc.GetDateTime();
        unsigned long difference = now.TotalSeconds() - (millis() / 1000);
        Serial.printf("[RTC] Time difference: %lu\n", difference);

        if (_rtc_difference == 0) {
            _rtc_difference = difference;
        } else if (difference != _rtc_difference) {
            long drift = _rtc_difference - difference;
            if (drift > 0) {
                now += drift;
            } else {
                now -= (-drift);
            }
            rtc.SetDateTime(now);
            Serial.printf("[RTC] Time drifted by %lu seconds\n", drift);
        }
    }

}

void rtcSetup() {

    unsigned char addresses[] = {0x68};
    unsigned char address = i2cFind(1, addresses);
    if (address == 0) return;

    _rtc_chip = rtcChip(address);
    Serial.printf("[RTC] %s a l'adreça 0x%02X\n", (_rtc_chip == RTC_DS1337) ? "RTC1337" : "RTC3231", address);

    rtc.Begin();
    RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
    if (!rtc.IsDateTimeValid()) {
        Serial.println("[RTC] El RTC no té informació vàlida, l'actualitzo");
        rtc.SetDateTime(compiled);
    }
    if (!rtc.GetIsRunning()) {
        Serial.println("[RTC] El RTC no estava funcionant, engengant-lo ara");
        rtc.SetIsRunning(true);
    }
    RtcDateTime now = rtc.GetDateTime();
    if (now < compiled) {
        Serial.println("[RTC] L'hora de l'RTC és més antiga que l'actual, l'actualitzo");
        rtc.SetDateTime(compiled);
    }

    rtc.Enable32kHzPin(false);
    rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone);

    Serial.printf("[RTC] L'hora de l'RTC és: %s\n", rtcDateTimeString().c_str());

}
