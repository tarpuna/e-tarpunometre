#define APP_NAME                "ENERGIA"
#define APP_VERSION             "1.0.18z"
#define APP_AUTHOR              "xose.perez@gmail.com & david@tarpunacoop.org"
#define APP_WEBSITE             "http://tarpunacoop.org"

// -----------------------------------------------------------------------------
// Configuració bàsica (descomenta i configura segons desitgis)
// -----------------------------------------------------------------------------

/*

Dispositiu CURRENT_HARDWARE, valors posibles:

ETARPUNOMETRE_0_5       0    //  Wemos D1 R2 inicial FC, amb únic led i sense boto
ETARPUNOMETRE_1_0       1    //  Wemos D1 R2 amb cercle de LEDs i boto
ETARPUNOMETRE_1_5       2    //  Wemos D1 R2 sense cercle de LEDs ni boto
TARPUNA_UNO_SHIELD_0_3  3    //  Wemos D1 R2 amb Tarpuna Shield v.0.3
ETARPUNOMETRE_2_0       4    //  Wemos D1 Esp-Wroom-02 Motherboard ESP8266 sense cercle de LEDs

*/

#ifndef CURRENT_HARDWARE
#define CURRENT_HARDWARE        ETARPUNOMETRE_1_0
#endif
