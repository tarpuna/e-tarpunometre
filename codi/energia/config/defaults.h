﻿//------------------------------------------------------------------------------
// GENERAL
//------------------------------------------------------------------------------

#define UPTIME_OVERFLOW             4294967295  // Uptime overflow value
#define MANUFACTURER                "TARPUNA"

//------------------------------------------------------------------------------
// DEBUG
//------------------------------------------------------------------------------

// Serial debug log

#ifndef DEBUG_SERIAL_SUPPORT
#define DEBUG_SERIAL_SUPPORT    1               // Enable serial debug log
#endif
#ifndef DEBUG_PORT
#define DEBUG_PORT              Serial          // Default debugging port
#endif

//------------------------------------------------------------------------------

// UDP debug log
// To receive the message son the destination computer use nc:
// nc -ul 8113

#ifndef DEBUG_UDP_SUPPORT
#define DEBUG_UDP_SUPPORT       0               // Enable UDP debug log
#endif
#define DEBUG_UDP_IP            IPAddress(192, 168, 1, 100)
#define DEBUG_UDP_PORT          8113

//------------------------------------------------------------------------------

// General debug options and macros
#define DEBUG_MESSAGE_MAX_LENGTH    80

#if (DEBUG_SERIAL_SUPPORT==1) || (DEBUG_UDP_SUPPORT==1)
    #define DEBUG_MSG(...) debugSend(__VA_ARGS__)
    #define DEBUG_MSG_P(...) debugSend_P(__VA_ARGS__)
#endif

#ifndef DEBUG_MSG
    #define DEBUG_MSG(...)
    #define DEBUG_MSG_P(...)
#endif

//------------------------------------------------------------------------------
// TERMINAL
//------------------------------------------------------------------------------

#ifndef TERMINAL_SUPPORT
#define TERMINAL_SUPPORT         1               // Enable terminal commands
#endif

// -----------------------------------------------------------------------------
// SETTINGS
// -----------------------------------------------------------------------------

#ifndef SETTINGS_AUTOSAVE
#define SETTINGS_AUTOSAVE       1           // Autosave settings o force manual commit
#endif

// -----------------------------------------------------------------------------
// HARDWARE
// -----------------------------------------------------------------------------

#define ETARPUNOMETRE_0_5       0
#define ETARPUNOMETRE_1_0       1
#define ETARPUNOMETRE_1_5       2    //  Wemos D1 R2 sense cercle de LEDs ni boto
#define TARPUNA_UNO_SHIELD_0_3  3
#define ETARPUNOMETRE_2_0       4    //  Wemos D1 Esp-Wroom-02 Motherboard ESP8266 sense cercle de LEDs

#ifndef CURRENT_HARDWARE
#define CURRENT_HARDWARE        ETARPUNOMETRE_1_5
#endif

#if CURRENT_HARDWARE == ETARPUNOMETRE_0_5
#define DEVICE_NAME                  "ETARPUNOMETRE_0_5"
#endif

#if CURRENT_HARDWARE == ETARPUNOMETRE_1_0
#define DEVICE_NAME                  "ETARPUNOMETRE_1_0"
#endif

#if CURRENT_HARDWARE == ETARPUNOMETRE_1_5
    #define DEVICE_NAME                  "ETARPUNOMETRE_1_5"
#endif

#if CURRENT_HARDWARE == TARPUNA_UNO_SHIELD_0_3
    #define DEVICE_NAME                  "TARPUNA_UNO_SHIELD_0_3"
#endif

#if CURRENT_HARDWARE == ETARPUNOMETRE_2_0
    #define DEVICE_NAME                  "ETARPUNOMETRE_2_0"
#endif

// -----------------------------------------------------------------------------
// GENERAL
// -----------------------------------------------------------------------------

#define SERIAL_BAUDRATE         115200
#define SENSOR_INTERVAL         6
#define SAVE_INTERVAL           600
#define SPIFFS_MIN_SIZE         14400

//------------------------------------------------------------------------------
// EEPROM
//------------------------------------------------------------------------------

#define EEPROM_SIZE             4096            // EEPROM size in bytes

// -----------------------------------------------------------------------------
// MACHINE STATES
// -----------------------------------------------------------------------------

#define STATE_LCD               0
#define STATE_RING              1

// -----------------------------------------------------------------------------
// I2C
// -----------------------------------------------------------------------------

#define I2C_SDA                 SDA
#define I2C_SCL                 SCL

// -----------------------------------------------------------------------------
// DISPLAY
// -----------------------------------------------------------------------------

// Address is dinamically assigned
#define LCD_ROWS                2
#define LCD_COLUMNS             16

// -----------------------------------------------------------------------------
// WIFI & WEB
// -----------------------------------------------------------------------------

#define WIFI_CONNECT_TIMEOUT    60000       // Connecting timeout for WIFI in ms
#define WIFI_RECONNECT_INTERVAL 300000      // If could not connect to WIFI, retry after this time in ms
#define WIFI_MAX_NETWORKS       3
#define HTTP_USERNAME           "admin"
#define ADMIN_PASS              "fibonacci"
#define DNS_PORT                53
#define WEBSERVER_PORT          80
#define WS_BUFFER_SIZE          5
#define WS_TIMEOUT              1800000
#ifndef WIFI_GAIN
#define WIFI_GAIN               0           // WiFi gain in dBm from 0 (normal) to 20.5 (max power and consumption)
#endif

#define WIFI_CORP_SUPPORT       1

// -----------------------------------------------------------------------------
// OTA
// -----------------------------------------------------------------------------

#define OTA_PORT                8266

// -----------------------------------------------------------------------------
// NOFUSS
// -----------------------------------------------------------------------------

#define NOFUSS_SUPPORT          1
#define NOFUSS_ENABLED          1
#define NOFUSS_SERVER           "http://tarpunacoop.org/nofuss/"
#define NOFUSS_INTERVAL         86400000

// -----------------------------------------------------------------------------
// LED
// -----------------------------------------------------------------------------

#define LED_MODE_BASIC          0 // Un únic led monocrom
#define LED_MODE_WS2822         1 // Un o més LEDs RGB (WS2812)

#if CURRENT_HARDWARE == ETARPUNOMETRE_0_5
    #define LED_MODE            LED_MODE_BASIC
    #define LED_PIN     		14        // D5
#elif CURRENT_HARDWARE == ETARPUNOMETRE_1_0
    #define LED_MODE            LED_MODE_WS2822
    #define LED_PIN     		14        // D5
    #define LED_COUNT           16
#elif CURRENT_HARDWARE == ETARPUNOMETRE_1_5
    #define LED_MODE            LED_MODE_BASIC
    #define LED_PIN     		14        // D5
    #define LED_COUNT           1
#elif CURRENT_HARDWARE == TARPUNA_UNO_SHIELD_0_3
    #define LED_MODE            LED_MODE_BASIC
    #define LED_PIN     		14
    #define LED_COUNT           1
#elif CURRENT_HARDWARE == ETARPUNOMETRE_2_0
    #define LED_MODE            LED_MODE_BASIC
    #define LED_PIN             LED_BUILTIN
#endif

#define BRIGHTNESS    			10 	    // Grau de brillantor en pertentatge

// -----------------------------------------------------------------------------
// BOTO SELECTOR
// -----------------------------------------------------------------------------

#if CURRENT_HARDWARE == ETARPUNOMETRE_2_0
    #define BUTTON_PIN 		    13	    // D7, pin on està connectat el botó selector
#else
    #define BUTTON_PIN 			12		// D6, pin on està connectat el botó selector
#endif

#define BUTTON_LONGCLICK        1000    // Premer el botó més d'1 segons és un click llarg

// -----------------------------------------------------------------------------
// DHT22
// -----------------------------------------------------------------------------

#define DHT_PIN                 13
#define DHT_TYPE                DHT22

// -----------------------------------------------------------------------------
// ENERGY MONITOR
// -----------------------------------------------------------------------------

#ifndef POWER_PHASES

#if CURRENT_HARDWARE == TARPUNA_UNO_SHIELD_0_3
	#define POWER_PHASES        1
    #else
	#define POWER_PHASES        3
#endif

#endif

#define POWER_SAMPLES           1000
#define POWER_CURRENT_RATIO     30.0
#define POWER_MAINS_VOLTAGE     230
#define POWER_MAX_POWER         3450
#define POWER_MIN_CURRENT       0.05

// INTERNAL ADC CONFIGURATION --------------------------------------------------
#define INTERNAL_REFERENCE_VOLTAGE      3.3
#define INTERNAL_ADC_BITS               10
#define INTERNAL_POWER_MODE             EMON_MODE_SAMPLES

// ADS1115 CONFIGURATION -------------------------------------------------------
// The ADC input range (or gain) can be changed via the following
// functions, but be careful never to exceed VDD +0.3V max, or to
// exceed the upper and lower limits if you adjust the input range!
// Setting these values incorrectly may destroy your ADC!
//                                                                ADS1015  ADS1115
//                                                                -------  -------
// ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
// ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
// ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
// ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
// ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
// ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV
#define ADS1115_GAIN                ADS1115_PGA_4P096   // GAIN_ONE
#define ADS1115_PORT_F1             ADS1115_MUX_P3_NG
#define ADS1115_PORT_F2             ADS1115_MUX_P2_NG
#define ADS1115_PORT_F3             ADS1115_MUX_P1_NG
#define ADS1115_REFERENCE_VOLTAGE   8.192               // range width peak to peak
#define ADS1115_ADC_BITS            16
#define ADS1115_POWER_MODE          EMON_MODE_MSECONDS

// MAX 10A =>
//#define ADS1115_GAIN            ADS1115_PGA_2P048   // GAIN_TWO
//#define POWER_REFERENCE_VOLTAGE 4.096               // range width peak to peak

// -----------------------------------------------------------------------------
// NTP
// -----------------------------------------------------------------------------

#define NTP_SERVER              "pool.ntp.org"
#define NTP_TIME_OFFSET         1
#define NTP_DAY_LIGHT           true
#define NTP_UPDATE_INTERVAL     1800

// -----------------------------------------------------------------------------
// MDNS
// -----------------------------------------------------------------------------

#ifndef MDNS_SUPPORT
#define MDNS_SUPPORT            1           // Enable MDNS by default
#endif

// -----------------------------------------------------------------------------
// FTP
// -----------------------------------------------------------------------------

#ifndef FTP_SUPPORT
#define FTP_SUPPORT             1
#endif
#ifndef FTP_ENABLED
#define FTP_ENABLED             0
#endif

#define FTP_HOST                ""
#define FTP_PORT                21
#define FTP_USER                ""
#define FTP_PASS                ""
#define FTP_PATH                ""
#define FTP_HOUR                20
