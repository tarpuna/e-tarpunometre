/*

MACHINE STATE MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include <vector>

typedef struct {
    unsigned char value;
    unsigned char max;
} state_t;

std::vector<state_t> _states;

unsigned char state(unsigned char id) {
    if (id < _states.size()) {
        return _states[id].value;
    }
    return 0;
}

unsigned char state(unsigned char id, unsigned char state) {
    if (id < _states.size()) {
        if (state < _states[id].max) {
            _states[id].value = state;
        }
        return _states[id].value;
    }
    return 0;
}

unsigned char stateNext(unsigned char id) {
    if (id < _states.size()) {
        _states[id].value = (_states[id].value + 1) % _states[id].max;
        return _states[id].value;
    }
    return 0;
}

void statesSetup() {
    #if CURRENT_HARDWARE == ETARPUNOMETRE_1_0
        _states.push_back((state_t) {0, 4}); // STATE_LCD
    #elif CURRENT_HARDWARE == ETARPUNOMETRE_1_5    
        _states.push_back((state_t) {0, 4}); // STATE_LCD
    #else
        _states.push_back((state_t) {0, 3}); // STATE_LCD
    #endif
    _states.push_back((state_t) {0, 3}); // STATE_RING
}
