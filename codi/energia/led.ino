/*

LED MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#if LED_MODE == LED_MODE_WS2822
    #include <Adafruit_NeoPixel.h>
    Adafruit_NeoPixel leds = Adafruit_NeoPixel(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
#endif

// -----------------------------------------------------------------------------
// LED
// -----------------------------------------------------------------------------

void ledSetup() {
    #if LED_MODE == LED_MODE_WS2822
        leds.begin();
        leds.setBrightness(255); // gestionem la brillantor píxel a píxel
    #else
        pinMode(LED_PIN, OUTPUT);
    #endif
}

#if LED_MODE == LED_MODE_WS2822

void ledTest() {

    // Posem tots els LEDs a negre
    for (int i=0; i<LED_COUNT; i++) leds.setPixelColor(i, 0);
    leds.show();

    for (int i=0; i<LED_COUNT; i++) {
        leds.setPixelColor(i, 255, 0, 0);
        leds.show();
        nice_delay(200);
    }

    for (int i=0; i<LED_COUNT; i++) {
        leds.setPixelColor(i, 0, 255, 0);
        leds.show();
        nice_delay(200);
    }

    for (int i=0; i<LED_COUNT; i++) {
        leds.setPixelColor(i, 0, 0, 255);
        leds.show();
        nice_delay(200);
    }

}

// Retorna el color del LED en funció de la posició
uint32_t getColor(unsigned int lednum) {

    unsigned char red = 0;
    unsigned char green = 0;
    unsigned char blue = 0;
    float brightness = getSetting("brightness", BRIGHTNESS).toFloat() / 100.0;

    unsigned char ringStatus = state(STATE_RING);

    if (ringStatus == 1) {
        if (2 == lednum) { red = 60; green = 255; blue = 60; }
        if (3 <= lednum && lednum <= 5) { red = 0; green = 255; blue = 0; }
        if (6 <= lednum && lednum <= 8) { red = 255; green = 255; blue = 0; }
        if (9 <= lednum && lednum <= 11) { red = 255; green = 128; blue = 0; }
        if (12 <= lednum && lednum <= 14) { red = 255; green = 0; blue = 0; }
    }

    if (ringStatus == 2) {
        red = 255;
        if (2 <= lednum && lednum <= 5) { red = 255; green = 255; blue = 255; }
        if (6 <= lednum && lednum <= 8) { red = 255; green = 200; blue = 100; }
        if (9 <= lednum && lednum <= 11) { red = 255; green = 100; blue = 50; }
    }

    return leds.Color((int) (brightness * red), (int) (brightness * green), (int) (brightness * blue));

}

void ledShow() {

    // Posem tots els LEDs a negre
    for (int i=0; i<LED_COUNT; i++) leds.setPixelColor(i, 0);

    // Il·luminem leds
    unsigned char ledmax = 0;
    bool ledblau = false;

    unsigned char ringStatus = state(STATE_RING);
    double temperatura = getTemperature();
    unsigned int humitat = getHumidity();

    if (ringStatus == 0) ledmax = 0;

    if (ringStatus == 1) {
        #if LED_COUNT > 3
            ledmax = constrain(map(temperatura, 18, 30, 1, LED_COUNT - 3), 1 , LED_COUNT - 3);
            ledblau = (temperatura < 18.0);
        #else
            // TODO: únic color per tots els LEDs
        #endif
    }


    if (ringStatus == 2) {

        int maxPower = getSetting("emonMaxPower", POWER_MAX_POWER).toInt();
        double ratio = getPowerNow(0) / maxPower;

        #if LED_COUNT > 3
            if (ratio <= 0.05) {
                ledmax = int(round(1+(ratio-0.01)*3/0.04));
            } else if (ratio <= 0.1) {
                ledmax = int(round(4+(ratio-0.05)*3/0.05));
            } else if (ratio <= 0.4) {
                ledmax = int(round(7+(ratio-0.10)*3/0.30));
            } else {
                ledmax = min(13, int(round(10+(ratio-0.40)*3/0.60)));
            }
        #else
            // TODO: únic color per tots els LEDs
        #endif

    }

    for (unsigned int i=0; i<ledmax; i++) {
        leds.setPixelColor(i+2, getColor(i+2));
    }

    #if LED_COUNT > 3

        float brightness = getSetting("brightness", BRIGHTNESS).toFloat() / 100.0;

        // Cas especial si temperatura < 18ºC
        if (ledblau) leds.setPixelColor(2, 0, 0, int(brightness * 255));

        // LED 0 per a WiFi ON/OFF
        if (wifiEnabled()) leds.setPixelColor(0, 0, 0, int(brightness * 255));

    	// LED 1 i 15 indiquen Confort HigroTèrmic
        int brilla = min(int(brightness * 255 * 3),255);
        if (ringStatus == 3) brilla = int(brightness * 255);
        if(19 <= temperatura && temperatura <=27 && 30 < humitat && humitat < 70) {
            leds.setPixelColor(1, 0, brilla, 0);
            leds.setPixelColor(15, 0);
        } else {
            leds.setPixelColor(1, 0);
            leds.setPixelColor(15, brilla, 0, 0);
        }

    #endif

    leds.show();

}

#else

void ledShow(bool status) {
    digitalWrite(LED_PIN, status);
}

#endif
