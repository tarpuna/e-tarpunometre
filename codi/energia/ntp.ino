/*

NTP MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include <TimeLib.h>
#include <NtpClientLib.h>
#include <WiFiClient.h>

// -----------------------------------------------------------------------------
// NTP
// -----------------------------------------------------------------------------

void ntpConnect() {
    NTP.begin(NTP_SERVER, NTP_TIME_OFFSET, NTP_DAY_LIGHT);
    NTP.setInterval(NTP_UPDATE_INTERVAL);
}

bool ntpConnected() {
    return (timeStatus() == timeSet);
}

String ntpDateTimeString() {
    //if (!ntpConnected()) return String("Not set");
    String value = NTP.getTimeDateString();
    int hour = value.substring(0, 2).toInt();
    int minute = value.substring(3, 5).toInt();
    int second = value.substring(6, 8).toInt();
    int day = value.substring(9, 11).toInt();
    int month = value.substring(12, 14).toInt();
    int year = value.substring(15, 19).toInt();
    char buffer[20];
    snprintf_P(buffer, sizeof(buffer), PSTR("%04d-%02d-%02d %02d:%02d:%02d"), year, month, day, hour, minute, second);
    return String(buffer);
}

void ntpSetup() {

    NTP.onNTPSyncEvent([](NTPSyncEvent_t error) {
        if (error) {
            if (error == noResponse) {
                DEBUG_MSG_P(PSTR("[NTP] Error: NTP server not reachable\n"));
            } else if (error == invalidAddress) {
                DEBUG_MSG_P(PSTR("[NTP] Error: Invalid NTP server address\n"));
            }
            wsSend("{\"ntpStatus\": false}");
        } else {

            DEBUG_MSG_P(PSTR("[NTP] Got time: %s\n"), (char *) ntpDateTimeString().c_str());

            {
                char buffer[64];
                snprintf(buffer, sizeof(buffer), "{\"ntpStatus\": true, \"time\": \"%s\"}", ntpDateTimeString().c_str());
                wsSend(buffer);
            }

            String value = NTP.getTimeDateString(NTP.getLastNTPSync());
            int hour = value.substring(0, 2).toInt();
            int minute = value.substring(3, 5).toInt();
            int second = value.substring(6, 8).toInt();
            int day = value.substring(9, 11).toInt();
            int month = value.substring(12, 14).toInt();
            int year = value.substring(15, 19).toInt();
            RtcDateTime datetime = RtcDateTime(year, month, day, hour, minute, second);
            rtcDateTime(datetime);

        }
    });

}

void ntpLoop() {
    now();
}
