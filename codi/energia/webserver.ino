/*

POWERNODE
WEBSERVER MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <Hash.h>
#include <AsyncJson.h>
#include <ArduinoJson.h>
#include <Ticker.h>
#include <vector>

#include "static/index.html.gz.h"

AsyncWebServer * _server;
AsyncWebSocket ws("/ws");
Ticker deferred;

typedef struct {
    IPAddress ip;
    unsigned long timestamp = 0;
} ws_ticket_t;
ws_ticket_t _ticket[WS_BUFFER_SIZE];
char _last_modified[50];
const char response[] PROGMEM = "{\"status\": \"OK\"}";

// -----------------------------------------------------------------------------
// WEBSOCKETS
// -----------------------------------------------------------------------------

void wsSend(const char * payload) {
    if (ws.count() > 0) {
        //DEBUG_MSG_P(PSTR("[WEBSOCKET] Broadcasting '%s'\n"), payload);
        ws.textAll(payload);
    }
}

void wsSend(uint32_t client_id, const char * payload) {
    DEBUG_MSG_P(PSTR("[WEBSOCKET] Sending '%s' to #%ld\n"), payload, client_id);
    ws.text(client_id, payload);
}

void _wsParse(uint32_t client_id, uint8_t * payload, size_t length) {

    // Parse JSON input
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject((char *) payload);
    if (!root.success()) {
        DEBUG_MSG_P(PSTR("[WEBSOCKET] Error parsing data\n"));
        ws.text(client_id, "{\"message\": \"Error parsing data!\"}");
        return;
    }

    // Check actions
    if (root.containsKey("action")) {

        String action = root["action"];
        DEBUG_MSG("[WEBSOCKET] Requested action: %s\n", action.c_str());

        if (action.equals("reset")) {
            ESP.restart();
        }

        if (action.equals("restore") && root.containsKey("data")) {

            JsonObject& data = root["data"];
            if (!data.containsKey("app") || (data["app"] != APP_NAME)) {
                ws.text(client_id, "{\"message\": \"The file does not look like a valid configuration backup.\"}");
                return;
            }

            for (unsigned int i = 0; i < SPI_FLASH_SEC_SIZE; i++) {
                EEPROM.write(i, 0xFF);
            }

            for (auto element : data) {
                if (strcmp(element.key, "app") == 0) continue;
                if (strcmp(element.key, "version") == 0) continue;
                setSetting(element.key, element.value.as<char*>());
            }

            saveSettings();

            ws.text(client_id, "{\"message\": \"Changes saved. You should reboot your board now.\"}");

        }

        if (action.equals("reconnect")) {

            // Let the HTTP request return and disconnect after 100ms
            deferred.once_ms(100, wifiDisconnect);

        }

        ws.text(client_id, "{\"message\": \"Acció executada!\"}");

    };

    // Check config
    if (root.containsKey("config") && root["config"].is<JsonArray&>()) {

        JsonArray& config = root["config"];
        DEBUG_MSG("[WEBSOCKET] Parsing configuration data\n");

        bool dirty = false;
        bool wifiX = false;
        unsigned int network = 0;

        for (unsigned int i=0; i<config.size(); i++) {

            String key = config[i]["name"];
            String value = config[i]["value"];

            if (key == "ssid") {
                key = key + String(network);
            }
            if (key == "pass") {
                key = key + String(network);
                ++network;
            }

            #if WIFI_CORP_SUPPORT
                if (key == "wifiX") {
                    wifiX = true;
                    continue;
                }
                if (key == "ssidX") {
                    if (value.length() == 0) continue;
                }
                if (key == "passX") {
                    if (value.length() == 0) continue;
                }
            #endif

            if (key == "timeNow") {
                int day = value.substring(0, 2).toInt();
                int month = value.substring(3, 5).toInt();
                int year = value.substring(6, 10).toInt();
                int hour = value.substring(11, 13).toInt();
                int minute = value.substring(14, 16).toInt();
                int second = value.substring(17, 19).toInt();
                RtcDateTime datetime = RtcDateTime(year, month, day, hour, minute, second);
                rtcDateTime(datetime);
                dirty = true;
                continue;
            }

            if (value != getSetting(key, "")) {
                setSetting(key, value);
                dirty = true;
            }

        }

        // checkboxes
        #if WIFI_CORP_SUPPORT
        if (!wifiX) {
        #endif
            delSetting("ssidX");
            delSetting("passX");
        #if WIFI_CORP_SUPPORT
        }
        #endif

        // Save settings
        if (dirty) {

            setSetting("hostname", cleanHostname(getSetting("hostname"), 14));

            wifiConfigure();
            powerConfigure();
            #if NOFUSS_SUPPORT
                nofussConfigure();
            #endif
            //registreConfigure();
            #if FTP_SUPPORT
                ftpConfigure();
            #endif

            saveSettings();

			ws.text(client_id, "{\"message\": \"Canvis desats\"}");

        } else {

            ws.text(client_id, "{\"message\": \"No hi havia cap canvi\"}");

        }

    }

}

void _wsStart(uint32_t client_id) {

    char chipid[7];
    sprintf(chipid, "%06X", ESP.getChipId());

    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();

    root["appname"] = APP_NAME;
    root["appversion"] = APP_VERSION;
    root["build"] = buildTime();
    root["manufacturer"] = String(MANUFACTURER);
    root["chipid"] = chipid;
    root["mac"] = WiFi.macAddress();
    root["device"] = String(DEVICE_NAME);
    root["hostname"] = getSetting("hostname");
    root["network"] = getNetwork();
    root["deviceip"] = getIP();
    root["time"] = rtcDateTimeString();
    root["uptime"] = getUptime();
    root["heap"] = ESP.getFreeHeap();
    root["sketch_size"] = ESP.getSketchSize();
    root["free_size"] = ESP.getFreeSketchSpace();
    root["ntpStatus"] = ntpConnected();

    char buffer_t[8];
    char buffer_c[8];
    dtostrf(getTemperature(), -5, 1, buffer_t);
    dtostrf(getCurrentNow(0), -6, 2, buffer_c);

    root["location"] = getSetting("location");
    root["hostname"] = getSetting("hostname");
    root["temperatura"] = String(buffer_t);
    root["humitat"] = getHumidity();
    root["potencia"] = getPowerNow(0);
    root["corrent"] = String(buffer_c);
    root["emonMaxPower"] = getSetting("emonMaxPower", POWER_MAX_POWER).toInt();
    if (hasADS1115()) root["fasesVisible"] = 1;
    root["voltatge"] = getVoltage();
    root["emonMains"] = getVoltage();
    root["emonRatio"] = getSetting("emonRatio", String(POWER_CURRENT_RATIO));
    root["numPhases"] =  getSetting("numPhases", POWER_PHASES).toInt();
    root["readInterval"] = getSetting("readInterval", String(SENSOR_INTERVAL));
    //root["logEnabled"] = registreEnabled();
    root["saveInterval"] = getSetting("saveInterval", String(SAVE_INTERVAL));
    root["brightness"] = getSetting("brightness", BRIGHTNESS);
    root["nofussEnabled"] = getSetting("nofussEnabled", NOFUSS_ENABLED).toInt() == 1;
    root["nofussServer"] = getSetting("nofussServer", NOFUSS_SERVER);

    #if BUTTON_PIN
        root["btnVisible"] = 1;
        root["btnType"] = getSetting("btnType", BUTTON_DEFAULT_HIGH).toInt();
    #endif

    #if FTP_SUPPORT
        root["ftpVisible"] = 1;
        root["ftpEnabled"] = getSetting("ftpEnabled", FTP_ENABLED).toInt() == 1;
        root["ftpHost"] = getSetting("ftpHost", FTP_HOST);
        root["ftpPort"] = getSetting("ftpPort", FTP_PORT).toInt();
        root["ftpUser"] = getSetting("ftpUser", FTP_USER);
        root["ftpPass"] = getSetting("ftpPass", FTP_PASS);
        root["ftpPath"] = getSetting("ftpPath", FTP_PATH);
        root["ftpHour"] = getSetting("ftpHour", FTP_HOUR).toInt();
    #endif

    JsonArray& wifi = root.createNestedArray("wifi");
    for (byte i=0; i<3; i++) {
        JsonObject& network = wifi.createNestedObject();
        network["ssid"] = getSetting("ssid" + String(i), "");
        network["pass"] = getSetting("pass" + String(i), "");
    }

    #if WIFI_CORP_SUPPORT
        root["wifiX"] = getSetting("ssidX").length() > 0;
        root["wifixVisible"] = 1;
    #endif

    String output;
    root.printTo(output);
    ws.text(client_id, (char *) output.c_str());

}

bool _wsAuth(AsyncWebSocketClient * client) {

    IPAddress ip = client->remoteIP();
    unsigned long now = millis();
    unsigned short index = 0;

    for (index = 0; index < WS_BUFFER_SIZE; index++) {
        if ((_ticket[index].ip == ip) && (now - _ticket[index].timestamp < WS_TIMEOUT)) break;
    }

    if (index == WS_BUFFER_SIZE) {
        DEBUG_MSG_P(PSTR("[WEBSOCKET] Validation check failed\n"));
        ws.text(client->id(), "{\"message\": \"Session expired, please reload page...\"}");
        return false;
    }

    return true;

}

void _wsEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len){

    static uint8_t * message;

    // Authorize
    #ifndef NOWSAUTH
        if (!_wsAuth(client)) return;
    #endif

    if (type == WS_EVT_CONNECT) {
        IPAddress ip = client->remoteIP();
        DEBUG_MSG_P(PSTR("[WEBSOCKET] #%u connected, ip: %d.%d.%d.%d, url: %s\n"), client->id(), ip[0], ip[1], ip[2], ip[3], server->url());
        _wsStart(client->id());
    } else if(type == WS_EVT_DISCONNECT) {
        DEBUG_MSG_P(PSTR("[WEBSOCKET] #%u disconnected\n"), client->id());
    } else if(type == WS_EVT_ERROR) {
        DEBUG_MSG_P(PSTR("[WEBSOCKET] #%u error(%u): %s\n"), client->id(), *((uint16_t*)arg), (char*)data);
    } else if(type == WS_EVT_PONG) {
        DEBUG_MSG_P(PSTR("[WEBSOCKET] #%u pong(%u): %s\n"), client->id(), len, len ? (char*) data : "");
    } else if(type == WS_EVT_DATA) {

        AwsFrameInfo * info = (AwsFrameInfo*)arg;

        // First packet
        if (info->index == 0) {
            message = (uint8_t*) malloc(info->len);
        }

        // Store data
        memcpy(message + info->index, data, len);

        // Last packet
        if (info->index + len == info->len) {
            _wsParse(client->id(), message, info->len);
            free(message);
        }

    }

}


// -----------------------------------------------------------------------------
// WEBSERVER
// -----------------------------------------------------------------------------

void webLogRequest(AsyncWebServerRequest *request) {
    DEBUG_MSG_P(PSTR("[WEBSERVER] Request: %s %s\n"), request->methodToString(), request->url().c_str());
}

bool _authenticate(AsyncWebServerRequest *request) {

    // demanar autenticació només si WIFI_STA
    if (WiFi.getMode() == WIFI_AP) return true;

    String password = getSetting("adminPass", ADMIN_PASS);
    char httpPassword[password.length() + 1];
    password.toCharArray(httpPassword, password.length() + 1);
    return request->authenticate(HTTP_USERNAME, httpPassword);

}

void _onLogs(AsyncWebServerRequest *request) {

    webLogRequest(request);
    if (!_authenticate(request)) return request->requestAuthentication();

    String path = "/logs/";
    Dir dir = SPIFFS.openDir(path);
    path = String();

    AsyncResponseStream *response = request->beginResponseStream("text/html");

    response->print("<html><body>");
    while (dir.next()){
        File f = dir.openFile("r");
        response->printf("<a target='_blank' href='%s'>%s</a><br />", f.name(), f.name());
        f.close();
    }

    response->print("</body></html>");
    request->send(response);

}

void _onClear(AsyncWebServerRequest *request) {

    webLogRequest(request);
    if (!_authenticate(request)) return request->requestAuthentication();

    String path = "/logs/";
    Dir dir = SPIFFS.openDir(path);
    while (dir.next()){
        File f = dir.openFile("r");
        SPIFFS.remove(f.name());
        f.close();
    }

    request->send_P(200, "text/html", response);

}

void _onAuth(AsyncWebServerRequest *request) {

    webLogRequest(request);
    if (!_authenticate(request)) return request->requestAuthentication();

    IPAddress ip = request->client()->remoteIP();
    unsigned long now = millis();
    unsigned short index;
    for (index = 0; index < WS_BUFFER_SIZE; index++) {
        if (_ticket[index].ip == ip) break;
        if (_ticket[index].timestamp == 0) break;
        if (now - _ticket[index].timestamp > WS_TIMEOUT) break;
    }
    if (index == WS_BUFFER_SIZE) {
        request->send(429);
    } else {
        _ticket[index].ip = ip;
        _ticket[index].timestamp = now;
        request->send(204);
    }

}

void _onGetConfig(AsyncWebServerRequest *request) {

    webLogRequest(request);
    if (!_authenticate(request)) return request->requestAuthentication();

    AsyncJsonResponse * response = new AsyncJsonResponse();
    JsonObject& root = response->getRoot();

    root["app"] = APP_NAME;
    root["version"] = APP_VERSION;

    unsigned int size = settingsKeyCount();
    for (unsigned int i=0; i<size; i++) {
        String key = settingsKeyName(i);
        String value = getSetting(key);
        root[key] = value;
    }

    char buffer[100];
    sprintf(buffer, "attachment; filename=\"%s-backup.json\"", (char *) getSetting("hostname").c_str());
    response->addHeader("Content-Disposition", buffer);
    response->setLength();
    request->send(response);

}

void _onHome(AsyncWebServerRequest *request) {

    webLogRequest(request);

    if (request->header("If-Modified-Since").equals(_last_modified)) {

        request->send(304);

    } else {

		AsyncWebServerResponse *response = request->beginResponse_P(200, "text/html", index_html_gz, index_html_gz_len);
		response->addHeader("Content-Encoding", "gzip");
		response->addHeader("Last-Modified", _last_modified);
		request->send(response);

	}

}

void webSetup() {

    // Create server
    _server = new AsyncWebServer(getSetting("webPort", WEBSERVER_PORT).toInt());

    // Setup websocket
    ws.onEvent(_wsEvent);

    // Cache the Last-Modifier header value
    sprintf(_last_modified, "%s %s GMT", __DATE__, __TIME__);

    // Setup webserver
    _server->addHandler(&ws);

    // Rewrites
    _server->rewrite("/", "/index.html");

    // Pages
    _server->on("/logs/list", HTTP_GET, _onLogs);
    _server->on("/clear", HTTP_GET, _onClear);
    _server->on("/index.html", HTTP_GET, _onHome);
    _server->on("/config", HTTP_GET, _onGetConfig);
    _server->on("/auth", HTTP_GET, _onAuth);

    // Static files
    _server->serveStatic("/", SPIFFS, "/")
        .setLastModified(_last_modified)
        .setFilter([](AsyncWebServerRequest *request) -> bool {
            webLogRequest(request);
            return true;
        });

    // 404
    _server->onNotFound([](AsyncWebServerRequest *request){
        request->send(404);
    });

    // Run server
    _server->begin();
    DEBUG_MSG_P(PSTR("[WEBSERVER] Webserver running on port %d\n"), getSetting("webPort", WEBSERVER_PORT).toInt());

}
