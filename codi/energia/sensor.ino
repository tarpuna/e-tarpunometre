/*

SENSOR MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include <DHT.h>
DHT * dht;

#include "SparkFun_Si7021_Breakout_Library.h"
Weather * si7021;

#include "ClosedCube_HDC1080.h"
ClosedCube_HDC1080 * hdc1080;

bool useDHT22 = false;
bool useHDC1080 = false;
bool useSI7021 = false;

double _temperatura;
unsigned char _humitat;

// -----------------------------------------------------------------------------
// HAL
// -----------------------------------------------------------------------------

double _getTemperature() {
    if (useDHT22) return dht->readTemperature();
    if (useSI7021) return si7021->getTemp();
    if (useHDC1080) return hdc1080->readTemperature();
    return 0;
}

unsigned char _getHumidity() {
    if (useDHT22) return dht->readHumidity();
    if (useSI7021) return si7021->getRH();
    if (useHDC1080) return hdc1080->readHumidity();
    return 0;
}

// -----------------------------------------------------------------------------

double getTemperature() {
    return _temperatura;
}

unsigned char getHumidity() {
    return _humitat;
}

void sensorLoop() {

    // Llegim les dades del sensor (humidity first always)
    double h = _getHumidity();
    double t = _getTemperature();

    // I comprovem si són vàlides (si són números)
    if (isnan(h) || isnan(t)) {

        Serial.println("[SENSOR] Error llegint el sensor DHT");

    } else {

        Serial.print("[SENSOR] Temperatura: ");
        Serial.println(t);
        Serial.print("[SENSOR] Humitat: ");
        Serial.println(h);
        _temperatura = t;
        _humitat = h;

    }

}

void sensorSetup() {

    unsigned char addresses[] = {0x40};
    unsigned char address = i2cFind(1, addresses);
    useDHT22 = (address == 0);

    if (useDHT22) {

        Serial.printf("[SENSOR] DHT22 al pin %d\n", DHT_PIN);
        dht = new DHT(DHT_PIN, DHT_TYPE);
        dht->begin();

    } else {

        si7021 = new Weather();
        si7021->begin();
        uint8_t id = si7021->checkID();
        if (id == 0x15) {
            Serial.printf("[SENSOR] SI7021 a l'adreça 0x%02X\n", address);
            useSI7021 = true;
        } else if (id == 0x32) {
            Serial.printf("[SENSOR] HTU21D a l'adreça 0x%02X\n", address);
            useSI7021 = true;
        } else {
            Serial.printf("[SENSOR] HDC1080 a l'adreça 0x%02X\n", address);
            useHDC1080 = true;
            delete si7021;
            hdc1080 = new ClosedCube_HDC1080();
            hdc1080->begin(address);
        }

    }

}
