# e-Tarpunòmetre - registre de canvis

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.8] 2018-09-02

(...)

## [1.0.5] 2017-08-31
### Changed
- New ESTAT field in log file

## [1.0.4] 2017-08-28
### Changed
- Improved NoFUSS support

## [1.0.3] 2017-08-25
### Changed
- Marcar per enviar des de la rutina de registre, sense alarmes

### Fixed
- Error a la data del registre

## [1.0.2] 2017-08-15
### Added
- Màquina d'estats per controlar l'LCD i l'anell de LEDs

### Changed
- Actualitzada llibreria EmonLiteESP
- Enregistrar als minuts exactes i amb timestamp del minut anterior

### Fixed
- Corregit error en no estar els settings inicialitzats quan arrenca emon.ino
- Corregit càlcul potència màxima minutal (un altre cop)
- Corregir enviament de dades via FTP quan l'arxiu és gran

## [1.0.1] 2017-07-29
### Added
- Afegit support per HDC1080
- Afegit configuració WIFI corporativa
- Afegida opció per deshabilitar NoFUSS
- Afegit support per pujar arxius via FTP

### Changed
- Canvis en el format del CSV (camps separats per punt i coma decimals amb coma)

### Fixed
- Corregit càlcul potència màxima minutal
- Corregit hostname, limitar longitud per evitar problemes a l'hora de guardar a SPIFFS

## [1.0.0] 2017-07-01
- Versió inicial alliberada
